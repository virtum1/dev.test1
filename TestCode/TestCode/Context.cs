﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCode
{
    public sealed class Context : DbContext
    {
        public DbSet<Lesson> Lessons { get; set; }
        public Context()
        {

        }
    }
}
