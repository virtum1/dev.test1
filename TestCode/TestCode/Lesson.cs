﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCode
{
    public class Lesson
    {
        public string LessonName { get; set; }
        public string EngDesc { get; set; }
        public string PolDesc { get; set; }
    }
}
