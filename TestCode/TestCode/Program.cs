﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestCode
{
    class Program
    {

        public static void Main()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            //var class1 = new Class1(5);
            //var class2 = new Class1(23, "Paweł");

            //var pawel = new Person<string, int>("Paweł", 23);
            //pawel.Show();

            //var pawulon = new Pochodna(22);
            //Console.WriteLine(pawulon.name + " " + pawulon.Age);

            //var rand = new Random();
            //var redList = new List<string>();
            //var blackList = new List<string>();

            //while (true)
            //{
            //    for (int i = 0; i < 10000; i++)
            //    {
            //        if (i <= 10000)
            //        {
            //            var r = rand.Next(0, 2);
            //            if (r == 0)
            //            {
            //                //Console.Write("Red-");
            //                redList.Add("r");
            //            }

            //            if (r == 1)
            //            {
            //                //Console.Write("Black-");
            //                blackList.Add("b");
            //            }
            //        }

            //    }
            //    Console.WriteLine("\n" + redList.Count);
            //    Console.WriteLine(blackList.Count);
            //    Console.ReadLine();
            //    //Thread.Sleep(3000);

            //    redList.Clear();
            //    blackList.Clear();
            //}


            //var item = new GenerciMethodsClass();
            ////item.DoSomething<string>("aaaa");
            ////item.DoSomething(5);

            //var tab = new int[2] { 1, 5 };
            //Console.WriteLine(tab.Sum());
            //Array.FindIndex(tab,
            //                value =>
            //                {
            //                    Console.WriteLine("Wartość to: {0}", value);
            //                    return value > 1;

            //                });
            //Console.WriteLine();


            //var src = new List<int>{5, 72, 10, 11, 9};

            //var target = Create<int, int>(src, n => n < 10, x => x * x);
            //foreach (var item in target)
            //{
            //    Console.WriteLine(item);
            //}

            Console.WriteLine("Podaj cztery liczby\nPierwsza: ");
            var x1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Druga: ");
            var x2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Trzecia: ");
            var x3 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Czwarta: ");
            var x4 = Convert.ToInt32(Console.ReadLine());

            var breaker = new CheckNumbers();
            Console.WriteLine("By złamać kod kliknij enter!");
            Console.ReadLine();
            breaker.LookForNumber(x1, x2, x3, x4);


            Console.ReadLine();
        }

        public class CheckNumbers
        {
            private int MatchNumber(int x, string arg)
            {
                var y = 0;
                for (int i = 0; i <= 10000; i++)
                {
                    Console.Write("\r{0}", i);
                    Thread.Sleep(1);
                    if (x == i)
                    {
                        y = i;
                        Console.WriteLine("\n{0} Gotowa!", arg);
                        break;
                    }
                }
                return y;
            }

            public void LookForNumber(int x1, int x2, int x3, int x4)
            {
                var y1 = 0;
                var y2 = 0;
                var y3 = 0;
                var y4 = 0;

                y1 = MatchNumber(x1, "Pierwsza");
                y2 = MatchNumber(x2, "Druga");
                y3 = MatchNumber(x3, "Trzecia");
                y4 = MatchNumber(x4, "Czwarta");

                Console.WriteLine("Hasło to: {0} : {1} : {2} : {3}", y1, y2, y3, y4);
            }
        }

        //public interface Filter<V>
        //{
        //    bool Test(V v);

        //}

        public delegate bool Filter<V>(V v);
        public delegate T Transformer<T, S>(S v);

        //public interface Transformer<T, S>
        //{
        //    T Transform(S v);
        //}

        public static List<T> Create<T, S>(List<S> src, Predicate<S> f, Transformer<T, S> t)
        {
            var target = new List<T>();
            foreach (var item in src)
            {
                if (f(item))
                {
                    target.Add(t(item));
                }
            }
            return target;
        }

        public class CustomerFluent
        {
            private Customer obj = new Customer();
            public CustomerFluent NameOfCustomer(string Name)
            {
                obj.FullName = Name;
                return this;
            }
            public CustomerFluent Bornon(string Dob)
            {
                obj.Dob = Convert.ToDateTime(Dob);
                return this;
            }
            public void StaysAt(string Address)
            {
                obj.Address = Address;

            }
        }

        public class Customer
        {
            private string _FullName;
            public string FullName
            {
                get { return _FullName; }
                set { _FullName = value; }
            }
            private DateTime _Dob;
            public DateTime Dob
            {
                get { return _Dob; }
                set { _Dob = value; }
            }
            private string _Address;
            public string Address
            {
                get { return _Address; }
                set { _Address = value; }
            }

        }

        public class GenerciMethodsClass
        {
            public void DoSomething<T>(T arg)
            {
                Console.WriteLine(arg);

            }
        }

        public class Nadrzedna
        {
            public string name;

            public Nadrzedna()
            {

            }

            public Nadrzedna(string name)
            {
                this.name = name;
            }

        }

        public class Pochodna : Nadrzedna
        {
            public int Age;

            public Pochodna(int age)
            {
                this.Age = age;
            }
        }

        public interface InterfaceTwo
        {

        }

        public interface InterfaceOne
        {

        }

        public class Person<String, Integer>
        {
            private string name;
            private int age;

            public Person(string name, int age)
            {
                this.name = name;
                this.age = age;
            }

            public void Show()
            {
                Console.WriteLine(name + " " + age);
            }

        }

        class Class1
        {
            private int value;
            private string name;

            public Class1(int value)
            {
                this.value = value;
            }

            public Class1(int age, string name)
                : this(age)
            {
                this.name = name;
            }

            public int MethodOne()
            {
                return 0;
            }

            public void MethodTwo(int i)
            {

            }
        }





        public class Vehicle
        {
            public virtual void Start()
            {
                Console.WriteLine("Metoda z klasy vehicle");
            }
        }

        public interface Speakable
        {

            string GetVoice(int voice);
        }

        public class Car : Vehicle
        {
            public override void Start()
            {
                Console.WriteLine("Metoda z klasy car");
            }
        }
    }
}
