﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class ChatMessage
    {
        public string Text { get; set; }
        public string Nick { get; set; }
        public DateTime Date { get; set; }
    }
}
