﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class ListClass
    {
        const string path = @"c:\temp\ConsoleServerLogs.txt";
        public List<Tuple<string, string, string>> server = new List<Tuple<string, string, string>>();
        const string itemsSeparator = " - ";

        public void Filter()
        {
            var hours = from hour in server
                        where hour.Item1 == "" || hour.Item2 == "" || hour.Item3 == "2014-04-26 22:38:17"
                        select hour;

            foreach (var item in hours)
            {
                Console.WriteLine(item);
            }
        }

        public void CheckList()
        {
            foreach (var item in server)
            {
                Console.WriteLine("{2} [{0}] - {1}", item.Item1, item.Item2, item.Item3);
            }
            Console.WriteLine("Koniec logów");
        }

        public void Save()
        {
            var fileInfo = new FileInfo(path);
            using (var stream = fileInfo.CreateText())
            {
                {
                    foreach (var item in server)
                    {
                        stream.WriteLine(String.Join(itemsSeparator, item.Item1, item.Item2, item.Item3));

                    }
                }
            }

        }

        public void Load()
        {

            var fileInfo = new FileInfo(path);
            if (!fileInfo.Exists) return;

            using (var stream = fileInfo.OpenText())
            {
                while (!stream.EndOfStream)
                {
                    var line = stream.ReadLine();
                    var items = line.Split(new[] { itemsSeparator }, StringSplitOptions.None);

                    var user = items[0];
                    var message = items[1];
                    var date = items[2];

                    Add(user, message, date);

                }
            }
        }

        public void Add(string user, string message, string date)
        {
            server.Add(Tuple.Create(user, message, date));
        }
    }
}
