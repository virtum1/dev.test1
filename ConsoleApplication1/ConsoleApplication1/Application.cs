﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Application : IDisposable
    {
        private readonly string userNick;
        private readonly ListClass listClass = new ListClass();


        public Application(string userNick)
        {
            this.userNick = userNick;
        }

        public void Run()
        {
           listClass.Load();


           while (true)
           {

               var message = Console.ReadLine();
               if (message.Equals("logs"))
               {
                   LoadLogs();
                   //listClass.CheckList();
               }
               else
               {
                   SendMessage(userNick, message);

                   listClass.Save();
               }

           }
        }
        public void LoadLogs()
        {
            var logs = listClass.server;
            Console.WriteLine("Oto logi z poprzedniej sesji");
            foreach (var item in logs)
            {
                Console.WriteLine("{2} [{0}] - {1}", item.Item1, item.Item2, item.Item3);
            }
        }

        private void SendMessage(string user, string message)
        {
            var msg = new ChatMessage { Nick = user, Text = message, Date = DateTime.Now};
            Console.WriteLine("{2} [{0}] - {1}", msg.Nick, msg.Text, msg.Date.ToString("yyyy-MM-dd HH:mm:ss"));
            listClass.Add(msg.Nick, msg.Text, msg.Date.ToString("yyyy-MM-dd HH:mm:ss"));
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }


    }
}
