﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1
{
    public sealed class Balance
    {
        public double Total { get; private set; }
        const string path = @"c:\temp\HomeBudgetProject.txt";
        public readonly List<BalanceEntry> payments = new List<BalanceEntry>();
        const string itemsSeparator = " - ";

        public void Add(double value, string description, DateTime date)
        {
            payments.Add(new BalanceEntry { Kwota = value, Opis = description, Data = date });
            Total += value;
        }

        public void Load()
        {

            var fileInfo = new FileInfo(path);
            if (!fileInfo.Exists) return;

            using (var stream = fileInfo.OpenText())
            {
                Load(stream);
            }
        }

        internal void Load(TextReader reader)
        {
            while (reader.Peek() != -1)
            {
                var line = reader.ReadLine();

                var items = line.Split(new[] { itemsSeparator }, StringSplitOptions.None);
                if (items.Length < 3) continue;

                var value = Convert.ToDouble(items[0]);
                var description = items[1];
                var date = Convert.ToDateTime(items[2]);

                Add(value, description, date);
            }
        }

        public void Save()
        {
            var fileInfo = new FileInfo(path);
            using (var stream = fileInfo.CreateText())
            {
                foreach (var item in payments)
                {
                    stream.WriteLine(String.Join(itemsSeparator, item.Kwota, item.Opis, item.Data));
                }
            }
        }
    }
}
