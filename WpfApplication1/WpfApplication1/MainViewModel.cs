﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;

namespace WpfApplication1
{
    public sealed class MainViewModel : PropertyChangedBase
    {
        private Balance balance;
        public ObservableCollection<BalanceEntry> Tasks { get; set; }
        public ICommand OnActivatedCommand { get; private set; }
        public CollectionViewSource CVS { get; set; }
        private DateTime yearMonth;
        public string YearMonth { get { return yearMonth.ToString("yyyy MMMM"); } }
        public DelegateCommand<object> PrevYearMonthCommand { get; set; }
        public DelegateCommand<object> NextYearMonthCommand { get; set; }
        public ICommand RefreshDataCommand { get; set; }

        public MainViewModel(Balance balance)
        {
            this.balance = balance;
        }

        public void Initialize()
        {
            OnActivatedCommand = new DelegateCommand<object>(OnActivated);

            Tasks = new ObservableCollection<BalanceEntry>();

            this.CVS = new CollectionViewSource();
            this.CVS.Source = this.Tasks;
            CVS.Filter += CVS_Filter;

            RefreshDataCommand = new DelegateCommand<object>(o => RefreshData());

            PrevYearMonthCommand = new DelegateCommand<object>(OnPrevYearMonth);
            NextYearMonthCommand = new DelegateCommand<object>(OnNextYearMonth);
            var now = DateTime.Now;
            yearMonth = new DateTime(now.Year, now.Month, 1);
        }

        void CVS_Filter(object sender, FilterEventArgs e)
        {
            var typedItem = e.Item as BalanceEntry;
            if (typedItem.Data.Year != yearMonth.Year)
            { 
                e.Accepted = false;
                return;
            }

            if (typedItem.Data.Month != yearMonth.Month)
            {
                e.Accepted = false;
                return;
            }

            e.Accepted = true;
        }
        

        private void RefreshData()
        {
            CVS.View.Refresh();
        }


        private void OnActivated(object param)
        {
            Tasks.Clear();
            foreach (var item in balance.payments)
            {
                Tasks.Add(item);
            }
        }

        private void OnPrevYearMonth(object parameter)
        {
            ChangeYearMonth(-1);

        }
        private void OnNextYearMonth(object parameter)
        {
            ChangeYearMonth(1);
        }

        private void ChangeYearMonth(int monthsDelta)
        {
            yearMonth = yearMonth.AddMonths(monthsDelta);
            NotifyOfPropertyChange(() => YearMonth);
            RefreshData();
        }
    }
}
