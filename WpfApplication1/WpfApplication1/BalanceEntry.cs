﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1
{
    public sealed class BalanceEntry : PropertyChangedBase
    {
        private double kwota;
        private string opis;
        private DateTime data;
        private bool modified;

        public double Kwota
        {
            get { return kwota; }
            set
            {
                kwota = value;
                NotifyOfPropertyChange(() => Kwota);
                Modified = true;
            }
        }

        public string Opis
        {
            get { return opis; }
            set
            {
                opis = value;
                NotifyOfPropertyChange(() => Opis);
                Modified = true;
            }
        }

        public DateTime Data
        {
            get { return data; }
            set
            {
                data = value;
                NotifyOfPropertyChange(() => Data);
                Modified = true;
            }
        }


        public bool Modified
        {
            get { return modified; }
            set
            {
                modified = value;
                NotifyOfPropertyChange(() => Modified);
            }
        }
    }
}
