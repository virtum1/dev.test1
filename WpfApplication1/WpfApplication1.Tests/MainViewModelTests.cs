﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1
{
    [TestFixture]
    public sealed class MainViewModelTests
    {
        [Test]
        public void ShouldInitializeCurrentPeriod()
        {
            var vm = new MainViewModel(new Balance());
            vm.Initialize();

            Assert.That(vm.YearMonth, Is.EqualTo(DateTime.Now.ToString("yyyy MMMM")));
        }

        [Test]
        public void ShouldShowCurrentPeriodData()
        {
            var dateFromCurrentPeriod = DateTime.Now;
            var balance = new Balance();
            balance.Add(1, "2", dateFromCurrentPeriod);

            var vm = new MainViewModel(balance);
            vm.Initialize();
            vm.OnActivatedCommand.Execute(null);

            var view = vm.CVS.View.Cast<BalanceEntry>();
            Assert.That(view.Any(), Is.True);
        }

        [Test]
        public void ShouldNotShowDateOutsideCurrentPeriod()
        {
            var dateFormPreviouseriod = DateTime.Now.AddMonths(-1);
            var balance = new Balance();
            balance.Add(1, "2", dateFormPreviouseriod);

            var vm = new MainViewModel(balance);
            vm.Initialize();
            vm.OnActivatedCommand.Execute(null);

            var view = vm.CVS.View.Cast<object>();
            Assert.That(view.Any(), Is.False);
        }
    }
}
