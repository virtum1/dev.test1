﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTesting
{
    public sealed class AutorizationProxy
    {
        private IAutorization service;

        public AutorizationProxy(IAutorization service)
        {
            this.service = service;
        }

        public bool CheckUser(string userName, string userPassword)
        {
            return service.CheckUser(userName, userPassword);
        }
    }
}
