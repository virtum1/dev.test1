﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTesting
{
    [TestFixture]
    public sealed class AutorizationProxyTests
    {
        [Test]
        public void ShouldBeTransparentForValidUser()
        {
            var service = new AutorizationStab();
            var proxy = new AutorizationProxy(service);
            Assert.IsTrue(proxy.CheckUser("John", "valid password"));
        }
    }

    class AutorizationStab : IAutorization
    {

        public bool CheckUser(string userName, string password)
        {
            return true;
        }
    }

}
