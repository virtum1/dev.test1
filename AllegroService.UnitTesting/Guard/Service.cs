﻿using AllegroService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guard
{
    public sealed class Service
    {
        private Dictionary<string, UserContext> userContexts = new Dictionary<string, UserContext>();
        private readonly IAutorizationService authorizationService;
        private readonly ITimer timer;

        public Service(IAutorizationService authorizationValidator)
            : this(authorizationValidator, new Timer())
        {

        }

        internal Service(IAutorizationService authorizationService, ITimer timer)
        {
            this.authorizationService = authorizationService;
            this.timer = timer;
        }
        public bool CheckUser(string userName, string password)
        {
            UserContext context;
            if (!userContexts.TryGetValue(userName, out context))
            {
                context = new UserContext();
                userContexts.Add(userName, context);
            }

            if (context.LoginAttempts >= 4)
            {
                if ((timer.Now - context.LockTimestamp) < TimeSpan.FromMinutes(5))
                    return false;

                context.LoginAttempts = 0;
            }

            if (authorizationService.IsValid(userName, password) == true)
            {
                userContexts[userName].LoginAttempts = 0;
                return true;
            }

            if (context.LoginAttempts++ == 3)
            {
                context.LockTimestamp = timer.Now;
            }

            return false;
        }

        class UserContext
        {
            public int LoginAttempts;

            public DateTime LockTimestamp;
        }
    }
}
