﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guard
{
    public interface ITimer
    {
        DateTime Now { get; }
    }
}
