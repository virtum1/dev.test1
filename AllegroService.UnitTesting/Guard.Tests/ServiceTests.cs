﻿using AllegroService;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guard
{
    [TestFixture]
    public sealed class ServiceTests
    {
        [Test]
        public void ShouldAuthorizeValidUser()
        {
            var authorizationProvider = Substitute.For<IAutorizationService>();
            authorizationProvider.IsValid("ValidUser", "ValidPassword").Returns(true);

            var service = new Service(authorizationProvider);
            var result = service.CheckUser("ValidUser", "ValidPassword");

            authorizationProvider.Received().IsValid("ValidUser", "ValidPassword");
            Assert.That(result, Is.EqualTo(true));

        }
        [Test]
        public void ShouldNotAuthorizeInvalidUSer()
        {
            var authorizationProvider = Substitute.For<IAutorizationService>();
            authorizationProvider.IsValid("ValidUser", "ValidPassword").Returns(false);

            var service = new Service(authorizationProvider);
            var result = service.CheckUser("InvalidUser", "InvalidPassword");
            Assert.That(result, Is.EqualTo(false));
        }

        /// <summary>
        ///  Done Jako użytkownik chcę aby moje konto zostało zablokowanie przy czwartym nieudanym pod rząd logowaniu tak, aby nie można było użyć automatu do łamania hasła na moim koncie użytkownika.
        /// </summary>
        [Test]
        public void ShouldBlockAccountOnFourthUnsuccessfulLoginInARow()
        {
            var authorizationProvider = Substitute.For<IAutorizationService>();
            authorizationProvider.IsValid("Jan", "Kowalski").Returns(true);
            Assert.That(authorizationProvider.IsValid("Jan", "Kowalski"));

            var service = new Service(authorizationProvider);
            service.CheckUser("Jan", "NieKowalski");
            service.CheckUser("Jan", "NieKowalski");
            service.CheckUser("Jan", "NieKowalski");
            service.CheckUser("Jan", "NieKowalski");

            Assert.That(!service.CheckUser("Jan", "Kowalski"));

        }

        /// <summary>
        /// Jako użytkownik chcę, aby pomyłkowe nieudane logowanie nie blokowało mojego konta tak, abym zachował ciągłość pracy nawet w przypadku rzadkich pomyłek w logowaniu.
        /// </summary>
        [Test]
        public void ShouldResetloginAttemptsAfterSuccesfullLogin()
        {
            var authorizationProvider = Substitute.For<IAutorizationService>();
            authorizationProvider.IsValid("Jan", "Kowalski").Returns(true);
            Assert.That(authorizationProvider.IsValid("Jan", "Kowalski"));

            var service = new Service(authorizationProvider);
            service.CheckUser("Jan", "NieKowalski");
            service.CheckUser("Jan", "NieKowalski");
            service.CheckUser("Jan", "Kowalski");
            service.CheckUser("Jan", "NieKowalski");
            service.CheckUser("Jan", "NieKowalski");

            Assert.That(service.CheckUser("Jan", "Kowalski"));
        }

        /// <summary>
        ///  Jako administrator chcę, aby blokada jednego konta nie blokowałą kont innych użytkowników.
        /// </summary>
        [Test]
        public void ShouldNotBlockedOtherUsersWhenTheCurrentUserIsBlocked()
        {
            var authorizationProvider = Substitute.For<IAutorizationService>();
            authorizationProvider.IsValid("Jan", "Kowalski").Returns(true);
            authorizationProvider.IsValid("Pawel", "Filocha").Returns(true);

            var service = new Service(authorizationProvider);
            service.CheckUser("Jan", "NieKowalski");
            service.CheckUser("Jan", "NieKowalski");
            service.CheckUser("Jan", "NieKowalski");
            service.CheckUser("Jan", "NieKowalski");

            Assert.That(service.CheckUser("Pawel", "Filocha"));
        }

        /// <summary>
        /// Jako użytkownik chcę aby moje zablokowane konto było odblokowane po 5ciu minutach, aby nie musiał prosić nikogo o jego odblokowanie (omówienie implementacji aktywnej i pasywnej).
        /// </summary>
        [Test]
        public void ShouldUnlockUserAfterFiveMinutes()
        {
            var authorizationProvider = Substitute.For<IAutorizationService>();
            authorizationProvider.IsValid("Jan", "Kowalski").Returns(true);
            var timer = Substitute.For<ITimer>();
            var now = DateTime.Now;
            timer.Now.Returns(now);

            var service = new Service(authorizationProvider, timer);
            service.CheckUser("Jan", "NieKowalski");
            service.CheckUser("Jan", "NieKowalski");
            service.CheckUser("Jan", "NieKowalski");
            service.CheckUser("Jan", "NieKowalski");

            Assert.That(!service.CheckUser("Jan", "Kowalski"));
            timer.Now.Returns(now.AddMinutes(5));
            Assert.That(service.CheckUser("Jan", "Kowalski"));
        }
    }
}
