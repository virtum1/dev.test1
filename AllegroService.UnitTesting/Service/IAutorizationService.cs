﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllegroService
{
    /// <summary>
    /// Checks if th user and passwor pais is valid.
    /// </summary>
    public interface IAutorizationService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns>True is user/password is correct, otherwise false.</returns>
        bool IsValid(string userName, string password);
    }
}
