﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Windows.Input;
using System.Linq;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Threading;
using System.Threading.Tasks;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Threading.Tasks;
using System.Reactive.Threading;

namespace AsyncUI
{
    class MainViewModel : INotifyPropertyChanged, ICommand
    {
        public ICommand OnKeyDownCommand { get; set; }
        public event EventHandler CanExecuteChanged;
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<string> Items { get; set; }
        HttpClient client = new HttpClient();
        private Subject<IObservable<string>> results = new Subject<IObservable<string>>();

        public MainViewModel()
        {
            OnKeyDownCommand = this;
            Items = new ObservableCollection<string>();
            results
                .Switch()
                .ObserveOn(SynchronizationContext.Current)
                .Subscribe(OnFilterResult);
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            var filter = parameter as string;
            var observableTask = client
                .GetStringAsync("http://maps.googleapis.com/maps/api/geocode/json?address=" + filter)
                .ToObservable();

            results.OnNext(observableTask);
        }

        private void OnFilterResult(string json)
        {
            var result = JsonConvert.DeserializeObject<RootObject>(json);
            var items = result.results.Select(m => m.formatted_address);

            Items.Clear();
            foreach (var item in items) Items.Add(item);
        }

    }
}
