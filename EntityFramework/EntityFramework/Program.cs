﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Diagnostics;
using System.Data.Entity.Migrations;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            //var dbMigrator = new DbMigrator(new Test.Migrations.Configuration());
            //dbMigrator.Update();

            Console.ForegroundColor = ConsoleColor.Green;

            using (var context = new DatabaseContext())
            {
                var a = context.Persons.Count();

                var sw = Stopwatch.StartNew();
                var item = context.Persons.FirstOrDefault();
                Debug.WriteLine(new TimeSpan(sw.ElapsedTicks).ToString());
                var person = new Person { Age = 22, Name = "apwel" };
                context.Persons.Add(person);
                context.SaveChanges();
            }

            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}
