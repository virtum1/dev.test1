﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{

    public class Person
    {
        public Person()
        {
            Children = new List<Person>();
        }

        //public int Id { get; set; }
        [Key]
        public int? Parent { get; set; }

        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Street { get; set; }

        [ForeignKey("Parent")]
        public virtual ICollection<Person> Children { get; set; }
    }
}
