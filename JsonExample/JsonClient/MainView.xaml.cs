﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JsonClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        HttpClient client = new HttpClient();
        Uri uri = new Uri("http://localhost:51132/jsonExample/api/values");
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            var item = "{ FirstName: '" + FirstName.Text + "', LastName: '" + LastName.Text + "'}";
            FirstName.Text = null;
            LastName.Text = null;
            var result = await client.PostAsync(uri, new StringContent(item, Encoding.UTF8, "application/json"));
           // Debug.Assert(result.StatusCode == HttpStatusCode.Created);

            var created = await client.GetStringAsync(result.Headers.Location);
            Info.Text = created;
        }
    }
}
