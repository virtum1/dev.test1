﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace JsonExample.Controllers
{
    public class ValuesController : ApiController
    {
        private static int currentId = 0;
        private static ConcurrentDictionary<int, PersonModel> cache = new ConcurrentDictionary<int, PersonModel>();

        // GET api/values
        public IEnumerable<PersonModel> Get()
        {
            return cache.Values;
        }

        // GET api/values/5
        public PersonModel Get(int id)
        {
            return cache[id];
        }

        // POST api/values
        public HttpResponseMessage Post([FromBody]PersonModel value)
        {
            //var id = Interlocked.Increment(ref currentId);
            //value.Id = id;
            //cache.TryAdd(id, value);
            using (var ctx = new DataContext())
            {
                var name = value.FirstName;
                var lastName = value.LastName;
                var person = new PersonModel { FirstName = name, LastName = lastName };
                ctx.Persons.Add(person);
                ctx.SaveChanges();
            }

            var response = Request.CreateResponse(HttpStatusCode.Created, value);

            var uri = Url.Link("DefaultApi", new { id = value.Id });
            response.Headers.Location = new Uri(uri);

            return response;
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        [HttpGet]
        public string Echo(string echo)
        {
            return echo;
        }
    }
}
