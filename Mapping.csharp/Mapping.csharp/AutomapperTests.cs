﻿using AutoMapper;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mapping
{
    [TestFixture]
    public sealed class AutomapperTests
    {
        [Test]
        public void ShouldMapProperties()
        {
            var dto = new PersonDto { Age = 1, FullName = "2" };

            Mapper.CreateMap<PersonDto, PersonModel>()
                .ForMember(t => t.Age, o => o.MapFrom(s => s.Age))
                .ForMember(t => t.Name, o => o.MapFrom(s => s.FullName));

            var model = Mapper.Map<PersonDto, PersonModel>(dto);

            Assert.IsTrue(model.Age == 1);
            Assert.AreEqual(model.Name, "2");
        }

        [Test]
        public void ShouldNotClone()
        {
            var dto = new PersonDto { Age = 1, FullName = "2" };

            var clone = Mapper.Map<PersonDto, PersonDto>(dto);

            Assert.That(dto, Is.SameAs(clone));
        }

    }
}
