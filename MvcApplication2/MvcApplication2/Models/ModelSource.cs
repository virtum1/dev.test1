﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MvcApplication2.Models
{
    public class ModelSource
    {
        public static Dictionary<string, Assembly> AwaitableAssemblies { get; private set; }

        static ModelSource()
        {
            AwaitableAssemblies = AppDomain.CurrentDomain.GetAssemblies()
                .GroupBy(a => a.GetName().Name)
                .ToDictionary(g => g.Key, g => g.First());
        }

        public static AssemblyModel FromName(string name)
        {
            Assembly asm;
            if (!AwaitableAssemblies.TryGetValue(name, out asm))
            {
                return null;
            }
            return new AssemblyModel(asm);
        }

    }
}
