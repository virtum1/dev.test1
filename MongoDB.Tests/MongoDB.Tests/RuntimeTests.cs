﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoDB
{
    [TestFixture]
    public class RuntimeTests
    {
        [Test]
        public void ShouldFindByObjectId()
        {
            var connectionString = "mongodb://herkules:maczuga@ds053380.mongolab.com:53380/sandbox";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var database = server.GetDatabase("sandbox"); // "test" is the name of the database

            var collection = database.GetCollection<Person>("people");
            var entity = new Person{ Name = "Pawel" };
            collection.Insert(entity);
            var id = entity.Id;

            var query = Query<Person>.EQ(e => e.Id, id);
            var foundEntity = collection.FindOne(query);

            Assert.That(entity.Name, Is.EqualTo(foundEntity.Name));
        }

        public class Person
        {
            public ObjectId Id;
            public string Name;
        }
    }
}
