﻿using Castle.DynamicProxy;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using NSubstitute;
using NUnit.Framework;
using System;

namespace AopTests
{
    [TestFixture]
    public sealed class ExceptionHandling
    {
        [Test]
        public void ShouldHandleException()
        {
            var container = new WindsorContainer();

            container.Register(
                Component.For<IServer>().ImplementedBy<Server>().Interceptors<ExceptionHandlerInterceptor>(),
                Component.For<ExceptionHandlerInterceptor>(),
                Component.For<IExceptionHandler>().Instance(Substitute.For<IExceptionHandler>()));

            var server = container.Resolve<IServer>();

            Assert.Throws<ArgumentException>(() => server.DoAction());

            var errorHandler = container.Resolve<IExceptionHandler>();
            errorHandler.Received().OnException();
        }
    }

    public class ExceptionHandlerInterceptor : IInterceptor
    {
        public IExceptionHandler ExceptionHandler { private get; set; }

        public void Intercept(IInvocation invocation)
        {
            try
            {
                invocation.Proceed();
            }
            catch
            {
                ExceptionHandler.OnException();
                throw;
            }
        }
    }

    public class Server : IServer
    {
        public void DoAction()
        {
            throw new ArgumentException();
        }
    }

    public interface IExceptionHandler
    {
        void OnException();
    }


    public interface IServer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="ArgumentException">
        /// bla bla bla
        /// </exception>
        void DoAction();
    }

}
