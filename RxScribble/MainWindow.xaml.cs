﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RxScribble
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            // Listing 11-19
            IObservable<EventPattern<MouseEventArgs>> downs =
                Observable.FromEventPattern<MouseEventArgs>(background, "MouseDown");
            IObservable<EventPattern<MouseEventArgs>> ups =
                Observable.FromEventPattern<MouseEventArgs>(background, "MouseUp");
            IObservable<EventPattern<MouseEventArgs>> allMoves =
                Observable.FromEventPattern<MouseEventArgs>(background, "MouseMove");

            IObservable<Point> dragPositions =
                from down in downs
                join move in allMoves
                  on ups equals allMoves // słowo on i equals w LINQ co oznacza?
                select move.EventArgs.GetPosition(background);

            // Kod został umieszczony w komentarzu, gdyż stanowi alternatywę dla wyrażenia zapytania
            // podanego na poprzednim listingu. Aby go wypróbować, należy umieścić w komentarzach
            // powyższą instrukcję.

            // Listing 11-20
            //IObservable<Point> dragPositions = downs.Join(
            //    allMoves,
            //    down => ups,
            //    move => allMoves,
            //    (down, move) => move.EventArgs.GetPosition(background));


            dragPositions.Subscribe(point => { line.Points.Add(point); });
        }

        // Listing 11-17
        private void OnBackgroundMouseDown(object sender, MouseButtonEventArgs e)
        {
            background.CaptureMouse();
        }

        private void OnBackgroundMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.Captured == background)
            {
                background.ReleaseMouseCapture();
            }
        }

    }
}
