﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EasyNetQ;
using RabbitMqChat.Contracts;
using RabbitMQ.Client;

namespace RabbitMqChat
{
    class Program
    {
        private static IBus _bus;
        static void Main(string[] args)
        {
            //var ip = Ask("Please enter server IP[:port]: ");
            var ip = "localhost";
            var user = Ask("Please enter your nick name: ");

            using (_bus = RabbitHutch.CreateBus("host=" + ip, x => x.Register<IEasyNetQLogger>(_ => new EmptyLogger())))
            {
                SendJoinedMessage(user);
                Console.WriteLine("You will be joined to chat soon. If you will want to leave just enter message 'exit'");
                SubscribeToMessages(user);
                SubscribeToCommand(user);
                while (true)
                {
                    Console.Write("> ");
                    var msg = Console.ReadLine();
                    

                    if (msg.Equals("exit", StringComparison.InvariantCultureIgnoreCase))
                    {
                        SendExitMessage(user);
                        break;
                    }
                    else
                    {
                        SendMessage(_bus, user, msg);
                    }
                }
            }
        }

        private static void ClearLine()
        {
            Console.Write(new string(' ', Console.BufferWidth - Console.CursorLeft));
        }

        private static void SubscribeToMessages(string user)
        {
            _bus.Subscribe<Joined>(user, msg => Console.WriteLine("User {0} joined at {1}", msg.User, msg.JoinedOn));
            _bus.Subscribe<Leaved>(user, msg => Console.WriteLine("User {0} left at {1}", msg.User, msg.LeftOn));
            _bus.Subscribe<Message>(user, msg =>
                {
                    Console.WriteLine("[{2}] {0}> {1}", msg.User, msg.Text, msg.PostedOn.ToString("HH:mm:ss"));
                    if (msg.Text.StartsWith(":delay"))
                        Thread.Sleep(1000);
                });
        }

        private static string Ask(string prompt)
        {
            string result = null;
            while (string.IsNullOrEmpty(result))
            {
                Console.Write(prompt);
                result = Console.ReadLine();
            }
            return result;
        }

        private static void SendJoinedMessage(string user)
        {
            using (var publishChannel = _bus.OpenPublishChannel())
            {
                publishChannel.Publish(new Joined { JoinedOn = DateTime.Now, User = user });
            }
        }

        private static void SendExitMessage(string user)
        {
            using (var publishChannel = _bus.OpenPublishChannel())
            {
                publishChannel.Publish(new Leaved() { LeftOn = DateTime.Now, User = user });
            }
        }

        private static void SendMessage(IBus bus, string user, string msg)
        {
            using (var publishChannel = bus.OpenPublishChannel())
            {
                publishChannel.Publish(new Message { PostedOn = DateTime.Now, User = user, Text = msg });
            }
        }

        private static void SubscribeToCommand(string user)
        {
            _bus.Respond<SampleRequest, SampleResponse>(request =>
                {
                    Console.WriteLine("Got request '{1}' from {0}. Sleeping before response...", request.User, request.Text);
                    Thread.Sleep(1000);
                    Console.WriteLine("Responding.");
                    return new SampleResponse() { Responder = user };
                });
        }
    }

    public class EmptyLogger : IEasyNetQLogger
    {
        public void DebugWrite(string format, params object[] args)
        {
        }

        public void InfoWrite(string format, params object[] args)
        {
        }

        public void ErrorWrite(string format, params object[] args)
        {
        }

        public void ErrorWrite(Exception exception)
        {
        }
    }
}
