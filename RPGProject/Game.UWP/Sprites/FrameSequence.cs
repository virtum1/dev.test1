﻿namespace TAS.Sprites
{
    public sealed class FrameSequence
    {
        public readonly int DefaultFrame;
        public readonly int[] Sequences;
        public readonly int RowNumber;
        public FrameSequence(int rowNumber, int defaultFrame, int[] sequence)
        {
            DefaultFrame = defaultFrame;
            RowNumber = rowNumber;
            Sequences = sequence;
        }
    }
}
