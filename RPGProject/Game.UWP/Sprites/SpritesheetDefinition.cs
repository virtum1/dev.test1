﻿namespace TAS.Sprites
{
    public sealed class SpritesheetDefinition
    {
        public string TextureAssetName { get; set; }
    }
}
