﻿using Microsoft.Xna.Framework;

namespace TAS.Sprites
{
    public sealed class SpriteContext
    {
        /// <summary>
        /// Index of sprite frame which need to be used at 'Draw' step.
        /// </summary>
        public int CurrentFrame;

        /// <summary>
        /// Sequence of all frames.
        /// </summary>
        public FrameSequence FrameSequence;

        /// <summary>
        /// How long (in miliseconds) is the current frame shown.
        /// </summary>
        public int FrameElapsedTime = 0;


        public Rectangle SourceRect;
    }

}
