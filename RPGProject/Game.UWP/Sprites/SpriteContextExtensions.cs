﻿using TAS.Client;
using TAS.Domain;
using TAS.Runtime;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TAS
{
    public static class TextureExtensions
    {
        public static Drawable ToDrawable(this Texture2D spritesheet, ScreenState screenState, Rectangle sourceRect, GeoPoint position, DrawCommand.ForTexture.GameLayer gameLayer)
        {
            return new Drawable(screenState, spritesheet, position, sourceRect, gameLayer);
        }

        public struct Drawable
        {
            private readonly Texture2D texture;
            private readonly Rectangle sourceRect;
            // point on screen where should be placed centr of sprite.
            private readonly GeoPoint geoPosition;
            private readonly DrawCommand.ForTexture.GameLayer gameLayer;

            private TextureRotation rotation;
            private ScreenState screenState;
            private Vector2 geoCenter;

            public Drawable(ScreenState screenState, Texture2D texture, GeoPoint position, Rectangle sourceRect, DrawCommand.ForTexture.GameLayer gameLayer)
            {
                this.texture = texture;
                this.geoPosition = position;
                this.sourceRect = sourceRect;
                this.gameLayer = gameLayer;
                this.screenState = screenState;
                this.rotation = TextureRotation.Original;
                this.geoCenter = default(Vector2);
            }

            public Drawable WithCameraView(Vector2 geoCenter)
            {
                this.geoCenter = geoCenter;
                return this;
            }

            public Drawable WithRotation(TextureRotation rotation)
            {
                this.rotation = rotation;
                return this;
            }

            public DrawCommand AsCommand()
            {
                var viewCentre = screenState.ScreenCenter;
                
                // delta (in GeoPoints) between sprite center position and camera center.
                var dgeo = new Vector2(geoPosition.X - geoCenter.X, geoPosition.Y - geoCenter.Y);
                var scaled_dgeo = dgeo * screenState.CellSize;

                scaled_dgeo = new Vector2(scaled_dgeo.X + screenState.ScreenCenter.X, scaled_dgeo.Y + screenState.ScreenCenter.Y);

                return new DrawCommand.ForTexture
                {
                    Texture = texture,
                    Position = scaled_dgeo,
                    SourceRect = sourceRect,
                    LayerDepth = gameLayer,
                    Rotation = rotation
                };
            }

        }

        /// <summary>
        /// Allows to define texture rotation
        /// so that can reuse texture in more scenarios.
        /// </summary>
        public enum TextureRotation
        {
            Original = 0,
            Right = 1,
            Turned = 2,
            Left = 3
        }
    }
}
