﻿using TAS.Runtime;

namespace TAS
{
    public interface IGameRunner
    {
        void Run(IGameLogic game);
    }
}
