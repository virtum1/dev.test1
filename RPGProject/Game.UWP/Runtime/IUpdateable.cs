﻿using Microsoft.Xna.Framework;

namespace TAS.Runtime
{
    public interface IUpdateable
    {
        void Update(GameTime gameTime);
    }
}