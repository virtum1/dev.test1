﻿using System;
using System.Reactive.Subjects;

namespace TAS.Runtime
{
    public sealed class Notification<T> : IObservable<T>, IObserver<T>
    {
        private readonly Subject<T> subject = new Subject<T>();

        public void OnCompleted()
        {
            subject.OnCompleted();
        }

        public void OnError(Exception error)
        {
            subject.OnError(error);
        }

        public void OnNext(T value)
        {
            subject.OnNext(value);
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            return subject.Subscribe(observer);
        }
    }
}
