﻿using Microsoft.Xna.Framework;
using System;

namespace TAS.Runtime.Monogame
{
    public sealed class GameTimeObserver : IUpdateable
    {
        public IObserver<GameUpdateNotification> GameTimeStream { get; set; }

        public void Update(GameTime gameTime)
        {
            var notification = new GameUpdateNotification();
            GameTimeStream.OnNext(notification);
        }
    }
}
