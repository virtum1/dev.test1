﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TAS.Runtime.Monogame
{
    public sealed class GameRunner : Microsoft.Xna.Framework.Game, IGameRunner
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public GameRunner()
        {
            graphics = new GraphicsDeviceManager(this);
            //graphics.IsFullScreen = true;
            IsMouseVisible = true;
            Content.RootDirectory = "Content";
        }

        IGameLogic gameLogic;
        public void Run(IGameLogic game)
        {
            gameLogic = game;

            // AFAIK this metod is invoked when game is alread initialized,
            gameLogic.Initialize();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            gameLogic.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.TransparentBlack);

            spriteBatch.Begin(sortMode: SpriteSortMode.FrontToBack);

            var command = gameLogic.Draw(gameTime);
            spriteBatch.Draw(command);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
