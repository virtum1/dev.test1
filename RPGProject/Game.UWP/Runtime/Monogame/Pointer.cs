﻿using TAS.Domain;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using TAS.Client;
using System.Reactive.Disposables;

namespace TAS.Runtime.Monogame
{
    /// <summary>
    /// Draws position of currently selected area.
    /// </summary>
    public sealed class Pointer : IDrawable, IUpdateable, IDisposable
    {
        public IContentProvider ContentProvider { get; set; }

        public IObserver<PointerPositionNotifiction> PointerPositionSender { get; set; }
        public IObservable<MouseNotificaton> MouseListener { get; set; }

        public IGeoView GeoView { get; set; }
        public IScreenProvider ScreenProvider { get; set; }

        public GeoPoint GeoPoint { get; set; }

        private Texture2D texture;
        private CompositeDisposable instanceDisposer = new CompositeDisposable();

        private Point mousePosition;

        public void Initialize()
        {
            texture = CreateSelectorTexture(ContentProvider);
            MouseListener.Subscribe(o => mousePosition = o.Position).DisposeWith(instanceDisposer);
        }

        private static Texture2D CreateSelectorTexture(IContentProvider contentProvider)
        {
            var texture = contentProvider.Create(34, 34);

            var size = 34;
            var colors = new Color[size * size];
            texture.GetData(colors);
            for (int x = 0; x < size; x++)
                for (int y = 0; y < size; y++)
                {
                    var color = Color.Transparent;
                    var i = y * size + x;
                    if (x == 0 || x == size - 1) color = Color.Red;
                    if (y == 0 || y == size - 1) color = Color.Red;

                    colors[i] = color;

                }
            texture.SetData(colors);
            return texture;
        }

        public DrawCommand Draw(GameDrawNotification gameTime)
        {
            return texture
                .ToDrawable(ScreenProvider.State, new Rectangle(0, 0, 34, 34), GeoPoint, DrawCommand.ForTexture.GameLayer.Cursor)
                .WithCameraView(GeoView.Center)
                .AsCommand();
        }

        public void Update(GameTime gameTime)
        {
            var state = ScreenProvider.State;
            // converts camera center position to values in pixels (related from 0,0)
            var camera_px = (int)(GeoView.Center.X * state.CellSize + state.ScreenCenter.X);
            var camera_py = (int)(GeoView.Center.Y * state.CellSize + state.ScreenCenter.Y);

            // convert mouse position to pixels location from (0,0)
            var dx = mousePosition.X - camera_px;
            var dy = mousePosition.Y - camera_py;

            // convert mouse position to georegion
            var geox = (int)Math.Round((float)dx / state.CellSize);
            var geoy = (int)Math.Round((float)dy / state.CellSize);

            GeoPoint = new GeoPoint(geox, geoy);
            PointerPositionSender.OnNext(new PointerPositionNotifiction { Pointed = GeoPoint });
        }

        public void Dispose()
        {
            instanceDisposer.Dispose();
        }
    }
}
