﻿using Microsoft.Xna.Framework.Graphics;

namespace TAS.Runtime.Monogame
{
    public sealed class ContentProviderProxy : IContentProvider
    {
        public Microsoft.Xna.Framework.Game Game { get; set; }

        public Texture2D Create(int width, int height)
        {
            return new Texture2D(Game.GraphicsDevice, width, height);
        }

        public T Load<T>(string assetName)
        {
            return Game.Content.Load<T>(assetName);
        }


    }
}
