﻿using Autofac;

namespace TAS.Runtime.Monogame
{
    public sealed class IocModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ContentProviderProxy>().As<ContentProviderProxy, IContentProvider>().SingleInstance();
            builder.RegisterType<MouseObserver>().As<IUpdateable>().PropertiesAutowired().SingleInstance();
            builder.RegisterType<Pointer>().As<IUpdateable, IDrawable>().PropertiesAutowired().OnActivated(c => c.Instance.Initialize()).SingleInstance();
            builder.RegisterType<GameTimeObserver>().As<IUpdateable>().PropertiesAutowired().SingleInstance();
            builder.RegisterType<ScreenProvider>().As<ScreenProvider, IScreenProvider>().SingleInstance();
        }
    }
}
