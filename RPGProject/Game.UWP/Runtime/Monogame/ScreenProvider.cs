﻿using Microsoft.Xna.Framework;

namespace TAS.Runtime.Monogame
{
    public sealed class ScreenProvider : IScreenProvider
    {
        public GameRunner GameRunner { get; set; }

        public ScreenState State { get; set; }

        // invoked manually after GameRunner is set.
        public void Initialize()
        {
            var screenCenter = new Point(
                (GameRunner.Window.ClientBounds.Width - GameRunner.Window.ClientBounds.Left) / 2,
                (GameRunner.Window.ClientBounds.Height - GameRunner.Window.ClientBounds.Top) / 2);
            var cellSize = 32;

            State = new ScreenState { ScreenCenter = screenCenter, CellSize = cellSize };
        }

    }
}
