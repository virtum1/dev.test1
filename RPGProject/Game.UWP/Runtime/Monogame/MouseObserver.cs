﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace TAS.Runtime.Monogame
{
    /// <summary>
    /// Converts mouse state to stream of observable notifications.
    /// </summary>
    public sealed class MouseObserver : IUpdateable
    {
        public IObserver<MouseNotificaton> Stream { get; set; }

        private MouseState lastState;
        public void Update(GameTime gameTime)
        {
            var state = Mouse.GetState();
            Stream.OnNext(new MouseNotificaton
            {
                Position = state.Position,
                MiddleButtonState = state.MiddleButton,
                LeftButtonPressed = state.LeftButton == ButtonState.Pressed && lastState.LeftButton != ButtonState.Pressed,
                MiddleButtonPressed = state.MiddleButton == ButtonState.Pressed && lastState.MiddleButton != ButtonState.Pressed
            });
        }
    }
}
