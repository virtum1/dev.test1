﻿using TAS.Runtime;
using static TAS.TextureExtensions;

namespace Microsoft.Xna.Framework.Graphics
{
    public static class SpriteBatchExtensions
    {
        public static void Draw(this SpriteBatch sb, DrawCommand command)
        {
            if (command is DrawCommand.ForGrouped) Draw(sb, (DrawCommand.ForGrouped)command);
            if (command is DrawCommand.ForTexture) Draw(sb, (DrawCommand.ForTexture)command);
        }

        private static void Draw(SpriteBatch sb, DrawCommand.ForGrouped command)
        {
            foreach (var cmd in command.Items()) sb.Draw(cmd);
        }

        private static void Draw(SpriteBatch sb, DrawCommand.ForTexture command)
        {
            var position = new Vector2(command.Position.X, command.Position.Y);

            float rotation;
            switch (command.Rotation)
            {
                case TextureRotation.Right:
                    rotation = MathHelper.Pi / 2;
                    break;
                case TextureRotation.Turned:
                    rotation = MathHelper.Pi;
                    break;
                case TextureRotation.Left:
                    rotation = - MathHelper.Pi / 2;
                    break;
                default:
                case TextureRotation.Original:
                    rotation = 0;
                    break;
            }

            var rotationPoint = new Vector2(command.SourceRect.Width / 2, command.SourceRect.Height / 2);
            sb.Draw(command.Texture, position, command.SourceRect, Color.White, rotation, rotationPoint
                , 1, SpriteEffects.None, ((int)command.LayerDepth) / 10f);
        }
    }
}
