﻿using Microsoft.Xna.Framework.Graphics;

namespace TAS.Runtime
{
    /// <summary>
    /// Allow to load assets from game resources.
    /// </summary>
    public interface IContentProvider
    {
        /// <summary>
        /// Loads an asset from resources.
        /// </summary>
        /// <typeparam name="T">Asset type (e.g. Texture2D)</typeparam>
        /// <param name="assetName">Asset name.</param>
        /// <returns>Loaded asset.</returns>
        T Load<T>(string assetName);

        Texture2D Create(int width, int height);
    }
}
