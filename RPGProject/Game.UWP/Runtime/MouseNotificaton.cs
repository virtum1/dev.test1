﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace TAS.Runtime
{
    public sealed class MouseNotificaton
    {
        public Point Position;
        public ButtonState MiddleButtonState;
        public bool MiddleButtonPressed;
        public bool LeftButtonPressed;
    }
}
