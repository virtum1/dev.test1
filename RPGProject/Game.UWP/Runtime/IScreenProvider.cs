﻿using Microsoft.Xna.Framework;

namespace TAS.Runtime
{
    /// <summary>
    /// Defines some screen factors in pixels.
    /// </summary>
    public interface IScreenProvider
    {
        /// <summary>
        /// Returns current state of Screen.
        /// </summary>
        ScreenState State { get; }
    }

    public struct ScreenState
    {
        /// <summary>
        /// Center of the screen (in pixels).
        /// </summary>
        public Point ScreenCenter { get; set; }

        /// <summary>
        /// Size (in pixels) of one single map cell (Width and Height).
        /// </summary>
        public int CellSize { get; set; }
    }
}
