﻿using Microsoft.Xna.Framework;

namespace TAS.Runtime
{
    /// <summary>
    /// Marks a game component as part which needs to be invoked in game loop with
    /// ability to draw something defined in DrawCommand.
    /// </summary>
    public interface IDrawable
    {
        /// <summary>
        /// Invoked when is possibility to add content to main game content.
        /// </summary>
        /// <param name="gameTime">Current game time.</param>
        DrawCommand Draw(GameDrawNotification gameTime);
    }
}
