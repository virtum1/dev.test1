﻿using Autofac;
using System;

namespace TAS.Runtime
{
    public sealed class IocModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(Notification<>)).As(typeof(IObservable<>), typeof(IObserver<>)).SingleInstance();
        }
    }
}
