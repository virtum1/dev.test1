﻿using System;

namespace TAS.Runtime
{
    public struct GameDrawNotification
    {
        public TimeSpan TotalGameTime;
    }
}
