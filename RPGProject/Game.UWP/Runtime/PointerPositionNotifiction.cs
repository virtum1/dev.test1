﻿using TAS.Domain;

namespace TAS.Runtime
{
    public sealed class PointerPositionNotifiction
    {
        public GeoPoint Pointed { get; set; }
    }
}
