﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using static TAS.TextureExtensions;

namespace TAS.Runtime
{
    public abstract class DrawCommand
    {
        /// <summary>
        /// Allows to group other commands.
        /// </summary>
        public sealed class ForGrouped : DrawCommand
        {
            private readonly List<DrawCommand> commands = new List<DrawCommand>();

            public ForGrouped Add(DrawCommand cmd)
            {
                commands.Add(cmd);
                return this;
            }

            public IEnumerable<DrawCommand> Items()
            {
                foreach (var item in commands)
                {
                    if (item is ForGrouped)
                    {
                        foreach (var item1 in ((ForGrouped)item).Items()) yield return item1;
                    }
                    else
                        yield return item;
                }
            }
        }

        public sealed class ForTexture : DrawCommand
        {
            public Texture2D Texture;
            public Rectangle SourceRect;
            // Screen coordinates for the centre of sprite.
            public Vector2 Position;
            public GameLayer LayerDepth = GameLayer.Ground;
            public TextureRotation Rotation = TextureRotation.Original;

            public enum GameLayer
            {
                Ground = 0,
                LandUnit = 1,
                Cursor = 10
            }
        }
    }
}
