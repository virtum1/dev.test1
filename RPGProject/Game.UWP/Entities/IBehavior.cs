﻿namespace TAS.Entities
{
    public interface IBehavior
    {
    }

    /// <summary>
    /// Behavior is an instance notified included
    /// in post-creation process for every created instance of <see cref="T"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBehavior<in T> : IBehavior
    {
        /// <summary>
        /// Invoked when T is created in game, before used them in any part of game.
        /// 
        /// Behavior can keep a reference to the instance, so need be informed 
        /// (thru returned Disposable) when instance need to be unregistered from the Behavior.
        /// </summary>
        /// <param name="instance"></param>
        void Handle(T instance);
    }
}
