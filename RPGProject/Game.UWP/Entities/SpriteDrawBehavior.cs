﻿using TAS.Client;
using TAS.Domain;
using TAS.Runtime;
using TAS.Sprites;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System.Reactive.Disposables;

namespace TAS.Entities
{
    /// <summary>
    /// General class to load and serve all sprite images.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public abstract class SpriteDrawBehavior<TEntity> : IBehavior<TEntity>, Runtime.IDrawable
        where TEntity : IEntity, new()
    {
        public IContentProvider ContentProvider { get; set; }
        public IScreenProvider ScreenProvider { get; set; }
        public IGeoView CameraView { get; set; }

        private List<TEntity> Entities = new List<TEntity>();

        private Rectangle Default;

        public void Initialize()
        {
            var conf = new Configuration();
            Configure(conf);

            var sheetPoint = new Point(conf.Default.Y * conf.SpriteSizePx, conf.Default.X * conf.SpriteSizePx);
            var spriteSize = new Point(conf.SpriteSizePx, conf.SpriteSizePx);
            Default = new Rectangle(sheetPoint, spriteSize);
        }

        Texture2D texture;
        public void Handle(TEntity item)
        {
            var def = GetSpreadsheetDefinition();
            texture = ContentProvider.Load<Texture2D>(def.TextureAssetName);

            item.InstanceDisposer.Add(texture);
            item.InstanceDisposer.Add(Disposable.Create(() => Entities.Remove(item)));

            Entities.Add(item);
        }

        protected abstract SpritesheetDefinition GetSpreadsheetDefinition();
        protected abstract GeoPoint GetPosition(TEntity entity);

        protected abstract void Configure(Configuration c);

        public DrawCommand Draw(GameDrawNotification gameTime)
        {
            var result = new DrawCommand.ForGrouped();
            foreach (var e in Entities)
            {
                var cmd = texture
                    .ToDrawable(ScreenProvider.State, Default, GetPosition(e), DrawCommand.ForTexture.GameLayer.LandUnit)
                    .WithCameraView(CameraView.Center)
                    .AsCommand();
                result.Add(cmd);
            }
            return result;
        }

        /// <summary>
        /// Allows to redefine default sprite in texture pack.
        /// 
        /// If not overwritten, assumed Point(0,0)
        /// </summary>
        /// <returns>Default sprite in 32bit coordinates</returns>
        public virtual Point DefaultRectangle()
        {
            return new Point(0, 0);
        }

        public class Configuration
        {
            public Point Default = new Point(0, 0);
            // Size of sprite in pixels (weight and height should be the same)
            public int SpriteSizePx = 32;
        }
    }
}

