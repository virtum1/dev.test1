﻿using TAS.Domain;
using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using TAS.Runtime;
using Microsoft.Xna.Framework.Graphics;
using TAS.Client;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace TAS.Entities
{
    /// <summary>
    /// Generic behavior responsible to attach instance behaviors and run them.
    /// 
    /// The behavior watches all existing entities. 
    /// </summary>
    public sealed class ArmourEntityBehavior : IBehavior<ArmourEntity>, Runtime.IDrawable, IDisposable
    {
        public IStrategy<ArmourEntity>[] Strategies { get; set; }
        public IObservable<PointerPositionNotifiction> PointerPositionListener { get; set; }
        public IObservable<MouseNotificaton> MouseListener { get; set; }
        public IObservable<GameUpdateNotification> GameUpdateListener { get; set; }

        private readonly CompositeDisposable instanceDisposer = new CompositeDisposable();
        private readonly List<ArmourEntity> entities = new List<ArmourEntity>();
        private GeoPoint pointed = new GeoPoint();

        public void Initialize()
        {
            Observable.CombineLatest(
                PointerPositionListener.Select(o => o.Pointed),
                GameUpdateListener,
                MouseListener.Select(o => o.LeftButtonPressed),
                UpdateModel.Create
                )
                .Subscribe(Update)
                .DisposeWith(instanceDisposer);
        }

        public void Handle(ArmourEntity instance)
        {
            entities.Add(instance);
        }

        sealed class UpdateModel
        {
            public GeoPoint Pointed { get; private set; }
            public GameUpdateNotification GameTime { get; private set; }
            public bool MouseLeftButtonPressed { get; private set; }

            public static UpdateModel Create(GeoPoint pointed, GameUpdateNotification gameTime, bool mouseLeftButtonPressed)
            {
                return new UpdateModel
                {
                    Pointed = pointed,
                    GameTime = gameTime,
                    MouseLeftButtonPressed = mouseLeftButtonPressed
                };
            }
        }

        private void Update(UpdateModel it)
        {
            foreach (var e in entities)
            {
                if (!it.MouseLeftButtonPressed) continue;
                if (!(pointed == e.Position)) continue;
                e.Selected = !e.Selected;
            }
        }

        public DrawCommand Draw(GameDrawNotification gameTime)
        {
            var result = new DrawCommand.ForGrouped();

            foreach (var e in entities)
                foreach (var s in Strategies)
                {
                    result.Add(s.Draw(e, gameTime));
                }

            return result;
        }

        public void Dispose()
        {
            instanceDisposer.Dispose();
        }
    }

    public interface IStrategy<TEntity>
    {
        DrawCommand Draw(TEntity entity, GameDrawNotification dameTime);
    }

    public sealed class ArmourSelectionDrawStrategy : IStrategy<ArmourEntity>
    {
        public IContentProvider ContentProvider { get; set; }
        public IScreenProvider ScreenProvider { get; set; }
        public IGeoView GeoView { get; set; }

        private Texture2D texture1;
        private Texture2D texture2;

        public void Initialize()
        {
            texture1 = CreateSelectorTexture(ContentProvider);
            texture2 = ContentProvider.Create(34, 34);
        }

        public DrawCommand Draw(ArmourEntity entity, GameDrawNotification gameTime)
        {
            if (!entity.Selected) return new DrawCommand.ForGrouped();

            var texture = gameTime.TotalGameTime.Milliseconds < 500 ? texture1 : texture2;

            return texture
                .ToDrawable(ScreenProvider.State, new Rectangle(0, 0, 34, 34), entity.Position, DrawCommand.ForTexture.GameLayer.Cursor)
                .WithCameraView(GeoView.Center)
                .AsCommand();
        }

        private static Texture2D CreateSelectorTexture(IContentProvider contentProvider)
        {
            var texture = contentProvider.Create(34, 34);

            var size = 34;
            var colors = new Color[size * size];
            texture.GetData(colors);
            for (int x = 0; x < size; x++)
                for (int y = 0; y < size; y++)
                {
                    var color = Color.Transparent;
                    var i = y * size + x;
                    if (x == 0 || x == size - 1) color = Color.Yellow;
                    if (y == 0 || y == size - 1) color = Color.Yellow;

                    colors[i] = color;

                }
            texture.SetData(colors);
            return texture;
        }

    }
}
