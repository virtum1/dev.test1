﻿using TAS.Domain;
using TAS.Sprites;
using Microsoft.Xna.Framework;

namespace TAS.Entities
{
    public sealed class ArmourSpriteDrawBehavior : SpriteDrawBehavior<ArmourEntity>
    {
        protected override SpritesheetDefinition GetSpreadsheetDefinition()
        {
            return new SpritesheetDefinition
            {
                TextureAssetName = "Units"
            };
        }

        protected override GeoPoint GetPosition(ArmourEntity entity)
        {
            return entity.Position;
        }

        protected override void Configure(Configuration c)
        {
            c.Default = new Point(3, 6);
        }
    }
}
