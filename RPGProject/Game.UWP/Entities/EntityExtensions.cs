﻿using TAS.Entities;

namespace TAS
{
    public static class EntityExtensions
    {
        public static T Use<T>(this IEntity entity)
        {
            return (T) entity.ContextItems[typeof(T)];
        }
    }
}
