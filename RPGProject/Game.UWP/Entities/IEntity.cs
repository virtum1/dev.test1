using System;
using System.Collections.Generic;
using System.Reactive.Disposables;

namespace TAS.Entities
{
    public interface IEntity
    {
        CompositeDisposable InstanceDisposer { get; }
        Dictionary<Type, object> ContextItems { get; }
    }
}
