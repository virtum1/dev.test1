﻿using TAS.Domain;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System.Reactive.Disposables;
using TAS.Runtime;
using System.Linq;
using TAS.Client;
using System;
using static TAS.TextureExtensions;

namespace TAS.Entities
{
    /// <summary>
    /// Draws an island.
    /// </summary>
    /// <remarks>
    /// IslandDrawBehavior handles each created island (because implements IBehavior<IslandEntity>)
    /// and is invoked to draw because of beeing Runtime.IDrawable.
    /// </remarks>
    public sealed class IslandDrawBehavior : IBehavior<IslandEntity>, Runtime.IDrawable
    {
        /// <summary>
        /// Required to load base txtures.
        /// </summary>
        public IContentProvider ContentProvider { get; set; }
        public IGeoView CameraView { get; set; }
        public IScreenProvider ScreenProvider { get; set; }

        private List<IslandEntity> Entities = new List<IslandEntity>();

        public void Handle(IslandEntity instance)
        {
            instance.InstanceDisposer.Add(Disposable.Create(() => Entities.Remove(instance)));

            var drawContext = ToDrawContext(instance, ContentProvider);
            instance.ContextItems.Add(drawContext.GetType(), drawContext);

            Entities.Add(instance);

            // for every entity, when initialized we need to create map to represent
            // every piece of terrain with proper texture.
        }

        /// <summary>
        /// Draws island terrain.
        /// </summary>
        /// <param name="gameTime"></param>
        /// <returns></returns>
        public DrawCommand Draw(GameDrawNotification gameTime)
        {
            var geoCenter = CameraView.Center;
            var result = new DrawCommand.ForGrouped();
            foreach (var item in Entities)
                result.Add(DrawEntity(item, ScreenProvider.State, geoCenter));
            return result;
        }

        private Random rnd = new Random();

        public static DrawCommand DrawEntity(IslandEntity entity, ScreenState screenState, Vector2 geoCenter)
        {
            var result = new DrawCommand.ForGrouped();
            var points = entity.Use<PointContext[]>();
            foreach (var p in points)
            {
                var cmd = p.Texture
                    .ToDrawable(screenState, new Rectangle(1, 1, 32, 32), p.Point, DrawCommand.ForTexture.GameLayer.Ground)
                    .WithCameraView(geoCenter)
                    .WithRotation(p.TextureRotation)
                    .AsCommand();
                result.Add(cmd);
            }
            return result;
        }

        private PointContext[] ToDrawContext(IslandEntity entity, IContentProvider contentProvider)
        {
            var plainTexture = ContentProvider.Load<Texture2D>("Terrain");

            // connect points to pairs
            var pairs = GeoPoints.AsPairs(entity.Edges);
            // create lines from pairs
            var lines = GeoPoints.AsLines(pairs).ToArray();
            // get point surrounded by lines
            var areas = GeoPoints.AsArea(lines);
            var rnd = new Random();

            var result = new List<PointContext>();
            foreach (var area in areas)
            {
                var item = new PointContext();
                item.Texture = plainTexture;
                item.TextureRotation = (TextureRotation)rnd.Next(4);
                item.Point = area;
                result.Add(item);
            }

            return result.ToArray();
        }

        /// <summary>
        /// Holds elements required to drwa single point of an island.
        /// </summary>
        struct PointContext
        {
            public GeoPoint Point;
            public Texture2D Texture;
            public TextureRotation TextureRotation;
        }
    }
}
