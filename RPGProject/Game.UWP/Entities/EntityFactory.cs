﻿namespace TAS.Entities
{
    public sealed class EntityFactory<TEntity> : IEntityFactory<TEntity>
        where TEntity : IEntity, new()
    {
        public Behaviors Behaviors { get; set; }

        public TEntity Create()
        {
            var entity = new TEntity();
            Behaviors.Handle(entity);

            return entity;
        }

        private static void instanceDisposer(TEntity entity)
        {
            entity.InstanceDisposer.Dispose();
            entity.ContextItems.Clear();
        }
    }
}
