﻿using Autofac;
using TAS.Domain;
using TAS.Runtime;

namespace TAS.Entities
{
    public sealed class IocModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ArmourEntityBehavior>().As<IBehavior, IDrawable>().PropertiesAutowired().OnActivated(it => it.Instance.Initialize()).SingleInstance();
            builder.RegisterGeneric(typeof(EntityFactory<>)).As(typeof(IEntityFactory<>)).PropertiesAutowired().SingleInstance();
            builder.RegisterType<Behaviors>().AsSelf().SingleInstance();
            builder.RegisterType<ArmourSpriteDrawBehavior>().As<IDrawable, IBehavior>().OnActivated(c => c.Instance.Initialize()).PropertiesAutowired().SingleInstance();
            builder.RegisterType<ArmourSelectionDrawStrategy>()
                .As<IStrategy<ArmourEntity>>()
                .OnActivated(it => it.Instance.Initialize())
                .PropertiesAutowired().SingleInstance();
            builder.RegisterType<IslandDrawBehavior>().As<IBehavior, IDrawable>().PropertiesAutowired().SingleInstance();
        }
    }
}
