﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace TAS.Entities
{
    public sealed class Behaviors
    {
        private readonly Dictionary<Type, Action<object>> allBehaviors;
        private readonly Dictionary<Type, Action<object>[]> behaviorsPerType = new Dictionary<Type, Action<object>[]>();


        public Behaviors(IBehavior[] behaviors)
        {
            this.allBehaviors = DiscoverBehaviors(behaviors);
        }

        public void Handle(object obj)
        {
            var typeBehaviors = default(Action<object>[]);
            foreach (var item in allBehaviors)
            {
                if (!behaviorsPerType.TryGetValue(obj.GetType(), out typeBehaviors))
                {
                    typeBehaviors = allBehaviors.Where(o => o.Key.IsInstanceOfType(obj)).Select(o => o.Value).ToArray();
                    behaviorsPerType[obj.GetType()] = typeBehaviors;
                }
            }

            foreach (var behavior in typeBehaviors) behavior(obj);
        }

        private static Dictionary<Type, Action<object>> DiscoverBehaviors(IBehavior[] behaviors)
        {
            var invoker = typeof(Behaviors)
                .GetMethod("Invoke", BindingFlags.Static | BindingFlags.NonPublic);

            var result = new Dictionary<Type, Action<object>>();
            foreach (var b in behaviors)
            {
                var behaviorType = b.GetType();
                var interfaces = behaviorType.GetInterfaces();
                foreach (var i in interfaces)
                {
                    if (!i.GetTypeInfo().IsGenericType) continue;
                    if (i.GetGenericTypeDefinition() != typeof(IBehavior<>)) continue;

                    var behaviorItemType = i.GetGenericArguments()[0];
                    var typedInvoker = invoker.MakeGenericMethod(behaviorItemType);

                    Action<object> typeBehaviors = null;
                    result.TryGetValue(behaviorItemType, out typeBehaviors);

                    typeBehaviors += (object o) =>
                    {
                        typedInvoker.Invoke(null, new object[] { b, o });
                    };
                    result[behaviorItemType] = typeBehaviors;
                }
            }

            return result;

        }

        private static void Invoke<T>(IBehavior<T> behavior, object entity)
        {
            behavior.Handle((T)entity);
        }

    }
}
