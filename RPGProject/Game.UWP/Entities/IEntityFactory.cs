﻿namespace TAS.Entities
{
    public interface IEntityFactory<out TEntity>
        where TEntity : IEntity, new()
    {
        TEntity Create();
    }
}
