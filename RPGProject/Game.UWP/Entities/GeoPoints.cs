﻿using TAS.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TAS.Entities
{
    public static class GeoPoints
    {
        /// <summary>
        /// Creates collection of ordered par from list of vertices.
        /// <summary>
        public static IEnumerable<Tuple<GeoPoint, GeoPoint>> AsPairs(GeoPoint[] points)
        {
            var itemsCount = points.Length;
            for (int i = 0; i < itemsCount; i++)
            {
                yield return Tuple.Create(points[i], points[(i + 1) % itemsCount]);
            }
        }

        /// <summary>
        /// Returns all mid-points between points from and to, excluding start and finish points.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="finish"></param>
        /// <returns>All mid-points between startPoint and endPoint</returns>
        public static IEnumerable<GeoPoint> AsLine(GeoPoint start, GeoPoint finish)
        {
            var dx = finish.X - start.X;
            var dy = finish.Y - start.Y;
            var stepx = dx > 0
                        ? 1
                        : dx < 0
                          ? -1
                          : 0;
            var stepy = dy > 0
                        ? 1
                        : dy < 0
                          ? -1
                          : 0;

            if (dy == 0)
            {
                for (int x = start.X + stepx; x < finish.X; x += stepx)
                    yield return new GeoPoint(x, start.Y);
            }
            else if (dx == 0)
            {
                for (int y = start.Y + stepy; y < finish.Y; y += stepy)
                    yield return new GeoPoint(start.X, y);
            }
            else yield break;
        }

        public static IEnumerable<GeoPoint> AsLines(IEnumerable<Tuple<GeoPoint, GeoPoint>> pairs)
        {
            foreach (var p in pairs)
            {
                yield return p.Item1;
                foreach (var item in AsLine(p.Item1, p.Item2))
                {
                    yield return item;
                }
                yield return p.Item2;
            }
        }

        // Creates sequence of area surrounded by border of points.
        public static IEnumerable<GeoPoint> AsArea(GeoPoint[] areaBorder)
        {
            var left = areaBorder.Min(it => it.X);
            var right = areaBorder.Max(it => it.X);
            var top = areaBorder.Min(it => it.Y);
            var bottom = areaBorder.Max(it => it.Y);
            var sortedRows = areaBorder
                .GroupBy(o => o.Y)
                .OrderBy(o => o.Key);

            foreach (var l in sortedRows)
            {
                var sortedPoints = l.OrderBy(o => o.X).ToArray();
                for (var i = 0; i < sortedPoints.Length - 2; i += 2)
                {
                    var pointFrom = sortedPoints[i];
                    var pointTo = sortedPoints[i + 1];
                    for (var x = pointFrom.X; x < pointTo.X; x++)
                        yield return new GeoPoint(x, pointFrom.Y);
                }
            }
        }
    }
}
