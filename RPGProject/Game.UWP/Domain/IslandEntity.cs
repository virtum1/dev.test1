using TAS.Entities;
using System;
using System.Collections.Generic;
using System.Reactive.Disposables;

namespace TAS.Domain
{
    public sealed class IslandEntity : IEntity
    {
        public CompositeDisposable InstanceDisposer { get; } = new CompositeDisposable();
        public Dictionary<Type, object> ContextItems { get; } = new Dictionary<Type, object>();
        public GeoPoint[] Edges { get; set; } = new[] { new GeoPoint(-5, -5), new GeoPoint(+5, -5), new GeoPoint(+5, +5), new GeoPoint(-5, +5) };
    }
}
