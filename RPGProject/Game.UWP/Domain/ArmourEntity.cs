﻿using TAS.Entities;
using System;
using System.Collections.Generic;
using System.Reactive.Disposables;

namespace TAS.Domain
{
    public sealed class ArmourEntity : IEntity
    {
        public CompositeDisposable InstanceDisposer { get; } = new CompositeDisposable();
        public Dictionary<Type, object> ContextItems { get; } = new Dictionary<Type, object>();
        public GeoPoint Position { get; set; }
        public bool Selected { get; set; }
    }
}
