﻿using System.Linq;
using System.Reactive.Linq;

namespace System
{
    public static class ObservableExtension
    {
        // http://stackoverflow.com/questions/2820685/get-previous-element-in-iobservable-without-re-evaluating-the-sequence
        public static IObservable<TResult> CombineWithPrevious<TSource, TResult>(
            this IObservable<TSource> source,
            Func<TSource, TSource, TResult> resultSelector)
        {
            return source.Scan(
                Tuple.Create(default(TSource), default(TSource)),
                (previous, current) => Tuple.Create(previous.Item2, current))
                .Select(t => resultSelector(t.Item1, t.Item2));
        }
    }
}
