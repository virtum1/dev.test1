﻿using Microsoft.Xna.Framework;

namespace TAS.Client
{
    /// <summary>
    /// Defines centre ov view (on GeoMap) and View size (width and heigh) 
    /// </summary>
    public interface IGeoView
    {
        /// <summary>
        /// A GeoPoint place pointed by the Camera.
        /// 
        /// Center coud be between two GeoPoints, so X and Y values are float, not int.
        /// </summary>
        Vector2 Center { get; set; }
    }
}
