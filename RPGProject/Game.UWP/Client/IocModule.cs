﻿using Autofac;

namespace TAS.Client
{
    public sealed class IocModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<GeoView>().As<IGeoView>().SingleInstance();
        }
    }
}
