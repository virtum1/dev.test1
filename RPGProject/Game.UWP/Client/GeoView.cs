﻿using TAS.Runtime;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Reactive.Disposables;

namespace TAS.Client
{
    public sealed class GeoView : IGeoView, IDisposable
    {
        public IScreenProvider ScreenProvider { private get; set; }
        public IObservable<MouseNotificaton> MouseStream { get; set; }
        public IObservable<GameUpdateNotification> GameTimeListener { get; set; }

        // Represents GeoPoint which is pointed by camera.
        public Vector2 Center { get; set; }

        public int ViewWidth { get; set; }
        public int ViewHeight { get; set; }

        private IState currentState;
        private IState scrollingState;
        private IState neutralState;
        private CompositeDisposable instanceDisposer = new CompositeDisposable();

        public void Initialize()
        {
            scrollingState = new ScrollingState { Base = this };
            neutralState = new NeutralState { Base = this };
            currentState = neutralState;
            MouseStream.Subscribe(o => middleButton = o.MiddleButtonState).DisposeWith(instanceDisposer);
            GameTimeListener.Subscribe(Update).DisposeWith(instanceDisposer);
        }

        ButtonState middleButton;
        ButtonState middleButtonPrevious;
        public void Update(GameUpdateNotification ignored)
        {
            if (middleButton != middleButtonPrevious)
            {
                // switching to map moving when middle button pressed.
                if (middleButton == ButtonState.Pressed)
                    currentState = scrollingState;
                else
                    currentState = neutralState;

                currentState.Activated();
            }

            currentState.Update();

            middleButtonPrevious = middleButton;
        }

        public void Dispose()
        {
            instanceDisposer.Dispose();
        }

        interface IState
        {
            void Activated();
            void Update();
        }

        /// <summary>
        /// * Move camera position with the same way as currently is moving mouse cursor.
        /// </summary>
        class ScrollingState : IState
        {
            public GeoView Base { private get; set; }

            Point mousePosition;
            public void Activated()
            {
                var mouseState = Mouse.GetState();
                mousePosition = mouseState.Position;
            }

            public void Update()
            {
                // change camera position based on mouse pointer.
                var currentMousePosition = Mouse.GetState().Position;
                var screenState = Base.ScreenProvider.State;
                var dx = (mousePosition.X - currentMousePosition.X) / (float)screenState.CellSize;
                var dy = (mousePosition.Y - currentMousePosition.Y) / (float)screenState.CellSize;

                Base.Center = new Vector2(Base.Center.X + dx, Base.Center.Y + dy);

                mousePosition = currentMousePosition;
            }
        }

        class NeutralState : IState
        {
            public GeoView Base { private get; set; }

            public void Activated()
            {
            }

            public void Update()
            {
            }
        }
    }
}
