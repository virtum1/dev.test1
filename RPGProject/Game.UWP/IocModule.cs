﻿using Autofac;
using TAS.Runtime;

namespace TAS
{
    public sealed class IocModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<GameLogic>().As<IGameLogic>().PropertiesAutowired().SingleInstance();
        }
    }
}
