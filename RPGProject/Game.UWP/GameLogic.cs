﻿using TAS.Entities;
using TAS.Domain;
using TAS.Runtime;

namespace TAS
{
    public sealed class GameLogic : IGameLogic
    {
        public IEntityFactory<ArmourEntity> ArmourFactory { get; set; }
        public IEntityFactory<IslandEntity> IslandFactory { get; set; }
        public IDrawable[] Drawables { get; set; }
        public IUpdateable[] Updateables { get; set; }

        private ArmourEntity armour;
        private IslandEntity island;

        public void Initialize()
        {
            armour = ArmourFactory.Create();
            island = IslandFactory.Create();
        }

        public DrawCommand Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            var result = new DrawCommand.ForGrouped();
            var time = new GameDrawNotification { TotalGameTime = gameTime.TotalGameTime };
            foreach (var item in Drawables)
            {
                var command = item.Draw(time);
                result.Add(command);
            }
            return result;
        }

        public void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            foreach (var item in Updateables)
            {
                item.Update(gameTime);
            }
        }
    }
}
