﻿using TAS.Domain;
using TAS.Entities;
using TAS.Runtime;
using TAS.Runtime.Monogame;
using System.Reflection;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Autofac;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace TAS
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GamePage : Page
    {
        readonly GameRunner _game;

        public GamePage()
        {
            this.InitializeComponent();

            // Create the game.
            var builder = new ContainerBuilder();
            //.RegisterAssemblyModules(GetType().GetTypeInfo().Assembly);
            builder.RegisterModule<TAS.Runtime.IocModule>();
            builder.RegisterModule<TAS.Runtime.Monogame.IocModule>();
            builder.RegisterModule<TAS.Client.IocModule>();
            builder.RegisterModule<TAS.IocModule>();
            builder.RegisterModule<TAS.Entities.IocModule>();
            var container = builder.Build();

            var launchArguments = string.Empty;
            _game = MonoGame.Framework.XamlGame<GameRunner>.Create(launchArguments, Window.Current.CoreWindow, swapChainPanel);

            var contentProvider =  container.Resolve<ContentProviderProxy>();
            contentProvider.Game = _game;

            var screenProvider = container.Resolve<ScreenProvider>();
            screenProvider.GameRunner = _game;
            screenProvider.Initialize();

            try
            {
                var gameLogic = container.Resolve<IGameLogic>();
                _game.Run(gameLogic);
            }
            catch (System.Exception e)
            {

                throw;
            }
        }
    }
}
