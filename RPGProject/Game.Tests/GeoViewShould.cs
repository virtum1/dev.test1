﻿using TAS.Client;
using Microsoft.Xna.Framework;
using TAS.Runtime;
using System.Reactive.Subjects;

namespace TAS
{
    public sealed class GeoViewShould
    {
        [Xunit.Fact]
        public void KeepCursorStableWhenScrolling()
        {
            var view = new GeoView();
            view.MouseStream = new Subject<MouseNotificaton>();
            view.GameTimeListener = new Subject<GameUpdateNotification>();
            view.Initialize();

            view.Center = new Vector2();
            view.Update(new GameUpdateNotification());
        }
    }
}
