﻿using NUnit.Framework;
using TAS.Domain;
using TAS.Entities;
using TAS.Runtime;

namespace TAS
{
    public sealed class IslandDrawBehaviorShould
    {
        [Xunit.Fact]
        public void DrawSimpleIsland()
        {
            var toDraw = new IslandEntity();
            var beh = new IslandDrawBehavior();
            var r = beh.Draw(default(GameDrawNotification));

            Assert.That(r, Is.Not.Null);
        }
    }
}
