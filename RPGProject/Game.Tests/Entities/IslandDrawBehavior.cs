﻿using TAS.Domain;
using TAS.Runtime;
using Microsoft.Xna.Framework;
using NUnit.Framework;
using System.Linq;
using Xunit;

namespace TAS.Entities
{
    public class IslandDrawBehaviorTests
    {
        [Fact]
        public void ShouldDrawTerrainBetweenIslandEdges()
        {
            var b = new IslandDrawBehavior();
            b.Handle(new IslandEntity { Edges = new[] { new GeoPoint(0, 0), new GeoPoint(10, 0) } });
            var command = (DrawCommand.ForGrouped) b.Draw(new GameDrawNotification());

            CollectionAssert.AreEqual(
                command.Items().OfType<DrawCommand.ForTexture>().Select(o => o.Position).ToList(),
                Enumerable.Range(0, 11).Select(o => new Point(o, 0)).ToList());
        }
    }
}
