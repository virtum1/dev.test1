﻿using TAS.Domain;
using NUnit.Framework;
using System;
using System.Linq;

namespace TAS.Entities
{
    public sealed class GeoPointsShould
    {
        [Xunit.Fact]
        public void CreateListOfPairsFromPoints()
        {
            var point11 = new GeoPoint(1, 1);
            var point14 = new GeoPoint(1, 4);
            var point34 = new GeoPoint(3, 4);
            var point31 = new GeoPoint(3, 1);
            var points = new[] { point11, point14, point34, point31 };

            var pairs = GeoPoints.AsPairs(points);
            Assert.That(pairs, Is.EquivalentTo(new[]
            {
                Tuple.Create(point11, point14),
                Tuple.Create(point14, point34),
                Tuple.Create(point34, point31),
                Tuple.Create(point31, point11)
            }));
        }

        [Xunit.Fact]
        public void CreateHorizontalLineFromPoints()
        {
            var from = new GeoPoint(1, 1);
            var to = new GeoPoint(5, 1);
            var line = GeoPoints.AsLine(from, to);

            Assert.That(line.Select(o => o.X), Is.EquivalentTo(Enumerable.Range(2, 3)));
            Assert.That(line.Select(o => o.Y), Is.EquivalentTo(Enumerable.Repeat(1, 3)));
        }

        [Xunit.Fact]
        public void CreateVerticalLineFromPoints()
        {
            var from = new GeoPoint(2, -3);
            var to = new GeoPoint(2, 2);
            var line = GeoPoints.AsLine(from, to);

            Assert.That(line.Select(o => o.X), Is.EquivalentTo(Enumerable.Repeat(2, 4)));
            Assert.That(line.Select(o => o.Y), Is.EquivalentTo(Enumerable.Range(-2, 4)));
        }
    }
}
