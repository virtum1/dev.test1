﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatClientForJava
{
    class Application : IDisposable
    {
        public Application(string userName)
        {

            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare("hello", false, false, false, null);

                    while (true)
                    {
                        var messageToSend = Console.ReadLine();
                        var body = Encoding.UTF8.GetBytes(messageToSend);

                        channel.BasicPublish("", "hello", null, body);
                        Console.WriteLine(" {1} - {0}", messageToSend, userName);
                    }
                }
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}

