﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {

        public class PowiesciOrazHistorie
        {
            public Historia[] tab = new Historia[10];
            public Historia this[int index]
            {
                get
                {
                    return tab[index];
                }
                set
                {
                    if (value != null)
                    {
                        tab[index] = value;
                    }
                }
            }
        }
        delegate double BinaryOperation(double x, double y);

        static void Main(string[] args)
        {
            //var file = new FilePusher(@"c:\temp\ConsoleServerLogs.txt");
            //var sub = new MySubscriber<string>();
            //file.Subscribe(sub);
            //Console.ReadLine();

            //var start = new KeyWatcher();
            //var sub = new MySubscriber<char>();
            //start.Subscribe(sub);
            //start.Run();

            
            //PowiesciOrazHistorie st = new PowiesciOrazHistorie();
            //st[9] = new Historia("Historia numer jeden");
            //st[0] = new Historia("Opowieść historia");

            //Console.WriteLine(st[0].ToString());
            //Console.WriteLine(st[9].ToString());
            //Console.WriteLine("{0}",st[0]);
            var op1 = 13.3;
            var op2 = 33.4;
            var factor = 15.02;
            BinaryOperation operation = (x, y) =>
            {
                return factor * ((x + y) / y);
            };
            var result = operation(op1, op2);

            Console.WriteLine(result);
            Console.ReadLine();
        }

    }
}
