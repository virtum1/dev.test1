﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    public sealed class KeyWatcher : IObservable<char>
    {
        private readonly List<Subscription> subscription = new List<Subscription>();

        public IDisposable Subscribe(IObserver<char> observer)
        {
            var sub = new Subscription(this, observer);
            subscription.Add(sub);
            return sub;
        }

        public void Run()
        {
            while (true)
            {
                char c = Console.ReadKey(true).KeyChar;
                foreach (Subscription sub in subscription.ToArray())
                {
                    sub.observer.OnNext(c);
                }
            }
        }
        public void RemoveSubscription(Subscription sub)
        {
            subscription.Remove(sub);
        }

    }
}
