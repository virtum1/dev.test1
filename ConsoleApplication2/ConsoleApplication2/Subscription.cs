﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication2
{
    public sealed class Subscription : IDisposable
    {
        private KeyWatcher keyWatcher;
        public IObserver<char> observer { get; set; }

        public Subscription(KeyWatcher keyWatcher, IObserver<char> observer)
        {
            this.keyWatcher = keyWatcher;
            this.observer = observer;
        }

        public void Dispose()
        {
            if (keyWatcher != null)
            {
                keyWatcher.RemoveSubscription(this);
                keyWatcher = null;
            }
        }
    }
}
