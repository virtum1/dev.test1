﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    public sealed class FilePusher : IObservable<string>
    {
        private string path;
        public FilePusher(string path)
        {
            this.path = path;
        }

        public IDisposable Subscribe(IObserver<string> observer)
        {
            using (var sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                    observer.OnNext(sr.ReadLine());
                }
            }
            observer.OnCompleted();
            return EmptyDisposable.Instance;
        }

        public sealed class EmptyDisposable : IDisposable
        {
            public static EmptyDisposable Instance = new EmptyDisposable();
            public void Dispose()
            {
                throw new NotImplementedException();
            }
        }
    }
}
