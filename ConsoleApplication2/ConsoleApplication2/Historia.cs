﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication2
{
    public class Historia
    {
        private string value;
        public Historia(string value)
        {
            this.value = value;
        }
        public override string ToString()
        {
            return value;
        }
    }
}
