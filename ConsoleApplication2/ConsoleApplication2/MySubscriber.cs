﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    public sealed class MySubscriber<T> : IObserver<T>
    {

        public void OnCompleted()
        {
            Console.WriteLine("Zakończono");
        }

        public void OnError(Exception error)
        {
            Console.WriteLine("Błąd: " + error);
        }

        public void OnNext(T value)
        {
            Console.WriteLine("Odebrana wartość: " + value);
        }
    }
}