﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanguageTests
{
    [TestFixture]
    public sealed class FunctionTests
    {
        [TestCase(2, 1 + 2)]
        [TestCase(3, 1 + 2 + 3)]
        [TestCase(4, 1 + 2 + 3 + 4)]
        public void ShouldAdd(int input, int expected)
        {
            Assert.That(Sum(input), Is.EqualTo(expected));
        }

        /// <summary>
        /// Sums values from 1 to x.
        /// </summary>
        private static int Sum(int x)
        {
            return SumIter(x, 0);
        }

        private static int SumIter(int x, int current)
        {
            if (x == 0) return current;
            else return SumIter(x - 1, current + x);
        }
    }
}
