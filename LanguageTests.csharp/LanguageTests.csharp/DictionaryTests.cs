﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanguageTests
{
    [TestFixture]
    public sealed class DictionaryTests
    {
        [Test]
        public void ShouldNotAddTheSameKeys()
        {
            var dictionary = new Dictionary<int, int>();
            dictionary.Add(1, 1);
            Assert.Throws<ArgumentException>(() => dictionary.Add(1, 2));
        }

        [Test]
        public void ShouldNotAddTheSameCustomKeys()
        {
            var dictionary = new Dictionary<SessionKey, int>();
            dictionary.Add(new SessionKey { Age = 1, Name = "2" }, 1);
            Assert.Throws<ArgumentException>(() => dictionary.Add(new SessionKey { Age = 1, Name = "2" }, 1));
        }
    }

    public sealed class SessionKey
    {
        public int Age { get; set; }
        public string Name { get; set; }
    }
}
