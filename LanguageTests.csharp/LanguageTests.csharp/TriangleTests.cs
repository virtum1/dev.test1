﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanguageTests
{
    [TestFixture]
    public sealed class TriangleTests
    {
        [TestCase(10, 9, 8, true)]
        [TestCase(8, 10, 9, true)]
        [TestCase(9, 8, 10, true)]
        [TestCase(1, 10, 1, false)]
        [TestCase(10, 1, 1, false)]
        [TestCase(1, 1, 10, false)]
        public void ShouldCreateTrinagle(int a, int b, int c, bool expected)
        {
            Assert.That(CanCreateTriangle(a, b, c), Is.EqualTo(expected));
        }

        public static bool CanCreateTriangle(int a, int b, int c)
        {
            return (a + b > c && b + c > a && c + a > b);
        }
    }
}
