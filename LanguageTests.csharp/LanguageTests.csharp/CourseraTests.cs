﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanguageTests
{
    /// <summary>
    /// for details refer to https://class.coursera.org/progfun-005/assignment/view?assignment_id=4
    /// </summary>
    [TestFixture]
    public sealed class CourseraTests
    {
        [TestCase("(if (zero? x) max (/ 1 x))", true)]
        [TestCase("I told him (that it’s not (yet) done). (But he wasn’t listening)", true)]
        [TestCase(":-)", false)]
        [TestCase("())(", false)]
        public void ShouldRecognize(string tested, bool expected)
        {
            Assert.That(Balance(tested), Is.EqualTo(expected));
        }

        private static bool Balance(string tested)
        {
            throw new NotImplementedException();
        }

    }
}
