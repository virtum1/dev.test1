﻿using NUnit.Framework;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Framing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Chat
{
    [TestFixture]
    public class RabbitTests
    {
        [Test]
        public void ShouldReceiveMessage()
        {
            var factory = new ConnectionFactory { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare("test", "fanout");

                var requestQueue = channel.QueueDeclare();
                channel.QueueBind(requestQueue, "test", "");
                var resetEvent = new ManualResetEvent(false);
                var consumer = new Consumer(resetEvent);
                channel.BasicConsume(requestQueue.QueueName, true, consumer);

                channel.BasicPublish("test", "", null, Encoding.UTF8.GetBytes("message1"));

                Assert.That(resetEvent.WaitOne(100), Is.True);
                Assert.That(consumer.ConsumedMessage, Is.EqualTo("message1"));
            }
        }

        [Test]
        public void ShouldOneClientReceiveResponse()
        {
            var response = string.Empty;

            var factory = new ConnectionFactory { };
            using (var client1 = factory.CreateConnection())
            using (var client2 = factory.CreateConnection())
            using (var server = factory.CreateConnection())
            using (var clientChannel1 = client1.CreateModel())
            using (var clientChannel2 = client2.CreateModel())
            using (var serverChannel = server.CreateModel())
            {
                const string requestExchangeName = "Chat.RabbitTests.ShouldReceiveResponse.Request";
                serverChannel.ExchangeDeclare(requestExchangeName, "direct", false, true, null);

                const string responseExchangeName = "Chat.RabbitTests.ShouldReceiveResponse.Response";
                clientChannel1.ExchangeDeclare(responseExchangeName, "direct", false, true, null);

                var serializer = new DataSerializer();


                var requestQueue = serverChannel.QueueDeclare();
                serverChannel.QueueBind(requestQueue.QueueName, requestExchangeName, string.Empty);
                var requestConsumer = new DelegateConsumer<EchoRequest>((o,s) =>
                {
                    serverChannel.BasicPublish(responseExchangeName, s, null, serializer.Serialize(new EchoResponse { Text = "cba" }));
                }, serializer);
                serverChannel.BasicConsume(requestQueue.QueueName, true, requestConsumer);


                var responseConsumer = new DelegateConsumer<EchoResponse>(o =>
                {
                    response += o.Text;
                }, serializer);

                var responseQueue1 = clientChannel1.QueueDeclare();
                var client1RoutingKey = Guid.NewGuid().ToString();
                clientChannel1.QueueBind(responseQueue1, responseExchangeName, client1RoutingKey);
                clientChannel1.BasicConsume(responseQueue1.QueueName, true, responseConsumer);

                var responseQueue2 = clientChannel2.QueueDeclare();
                clientChannel2.QueueBind(responseQueue2, responseExchangeName, Guid.NewGuid().ToString());
                clientChannel2.BasicConsume(responseQueue2.QueueName, true, responseConsumer);

                clientChannel1.BasicPublish(requestExchangeName,
                    string.Empty,
                    new BasicProperties { ReplyTo = client1RoutingKey },
                    serializer.Serialize(new EchoRequest { Text = "abc" }));

                Thread.Sleep(100);
            }

            Assert.That(response, Is.EqualTo("cba"));
        }

        public class EchoRequest
        {
            public string Text { get; set; }
        }

        public class EchoResponse
        {
            public string Text { get; set; }
        }

        class Consumer : IBasicConsumer
        {
            public event ConsumerCancelledEventHandler ConsumerCancelled;

            public string ConsumedMessage { get; private set; }

            private EventWaitHandle resetEvent;
            public Consumer(EventWaitHandle resetEvent)
            {
                this.resetEvent = resetEvent;
            }

            public void HandleBasicCancel(string consumerTag)
            {
                throw new NotImplementedException();
            }

            public void HandleBasicCancelOk(string consumerTag)
            {
                throw new NotImplementedException();
            }

            public void HandleBasicConsumeOk(string consumerTag)
            {

            }

            public void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
            {
                ConsumedMessage = Encoding.UTF8.GetString(body);
                resetEvent.Set();
            }

            public void HandleModelShutdown(IModel model, ShutdownEventArgs reason)
            {
            }

            public IModel Model
            {
                get { throw new NotImplementedException(); }
            }
        }
    }
}
