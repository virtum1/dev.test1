﻿using Chat;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Chat
{
    [TestFixture]
    public class DataSerializerTests
    {
        [Test]
        public void ShouldClone()
        {
            var message = new ChatMessage { Nick = "my nick", Text = "any text", ApplicationToken = Guid.NewGuid() };

            var serializer = new DataSerializer();
            var clone = serializer.Deserialize<ChatMessage>(serializer.Serialize(message));

            Assert.That(message.Nick, Is.EqualTo(clone.Nick));
            Assert.That(message.Text, Is.EqualTo(clone.Text));
            Assert.That(message.ApplicationToken, Is.EqualTo(clone.ApplicationToken));
        }
    }
}
