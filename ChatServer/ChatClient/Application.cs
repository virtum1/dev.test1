﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Chat
{
    public sealed class Application : IDisposable
    {
        private DataSerializer serializer = new DataSerializer();
        private readonly IConnection connection;
        private readonly IModel chatModel;
        private readonly IModel historyModel;
        private readonly string userNick;
        private Guid applicationToken = Guid.NewGuid();
        private readonly ServerLogs serverLogs = new ServerLogs();

        public Application(string userNick)
        {
            this.userNick = userNick;

            var connectionFactory = new ConnectionFactory() { HostName = "localhost" };
            connection = connectionFactory.CreateConnection();
            chatModel = connection.CreateModel();
            chatModel.ExchangeDeclare("Chat", "fanout");

            historyModel = connection.CreateModel();
            historyModel.ExchangeDeclare("History", "fanout");

            var incomingMessages = chatModel.QueueDeclare();
            chatModel.QueueBind(incomingMessages.QueueName, "Chat", string.Empty);

            var consumer = new DelegateConsumer<ChatMessage>(OnMessage, serializer);
            chatModel.BasicConsume(incomingMessages.QueueName, true, consumer);

        }

        public void Run()
        {
            historyModel.BasicPublish("History", string.Empty, null, serializer.Serialize(new InitialChatRequest()));
            serverLogs.Load();

            while (true)
            {
                var message = Console.ReadLine();
                if (message.Equals("logs"))
                {
                    LoadLogs();
                }
                else
                {
                    SendMessage(userNick, message);
                }
            }
        }

        private void SendMessage(string user, string message)
        {
            var msg = new ChatMessage { Nick = user, Text = message, ApplicationToken = applicationToken, Date = DateTime.Now };

            chatModel.BasicPublish("Chat", "", null, serializer.Serialize(msg));
        }

        private void OnMessage(ChatMessage message)
        {
            if (message.ApplicationToken == applicationToken)
            {
                serverLogs.Add(message.Nick, message.Text, message.Date.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            else
            {
                Console.WriteLine("{2} [{0}] - {1}", message.Nick, message.Text, message.Date.ToString("yyyy-MM-dd HH:mm:ss"));
                serverLogs.Add(message.Nick, message.Text, message.Date.ToString("yyyy-MM-dd HH:mm:ss"));
            }
        }

        public void LoadLogs()
        {
            var logs = serverLogs.server;
            Console.WriteLine("Oto logi z poprzedniej sesji");
            foreach (var item in logs)
            {
                Console.WriteLine("{2} [{0}] - {1}", item.Item1, item.Item2, item.Item3);
            }
            Console.WriteLine("Koniec logów");
        }


        public void Dispose()
        {
            chatModel.Dispose();
            connection.Dispose();
        }
    }

}
