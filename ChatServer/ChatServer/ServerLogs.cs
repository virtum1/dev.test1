﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat
{
    class ServerLogs
    {
        const string path = @"c:\temp\ServerLogs.txt";
        public List<Tuple<string, string, string>> server = new List<Tuple<string, string, string>>();
        const string itemsSeparator = " - ";

        public void CheckList()
        {
            foreach (var item in server)
            {
                Console.WriteLine("{2} [{0}] - {1}", item.Item1, item.Item2, item.Item3);
            }
        }

        public void Save()
        {
            var fileInfo = new FileInfo(path);
            using (var stream = fileInfo.CreateText())
            {
                {
                    foreach (var item in server)
                    {
                        stream.WriteLine(String.Join(itemsSeparator, item.Item1, item.Item2, item.Item3));

                    }
                }
            }

        }

        public void Load()
        {

            var fileInfo = new FileInfo(path);
            if (!fileInfo.Exists) return;

            using (var stream = fileInfo.OpenText())
            {
                while (!stream.EndOfStream)
                {
                    var line = stream.ReadLine();
                    var items = line.Split(new[] { itemsSeparator }, StringSplitOptions.None);

                    var user = items[0];
                    var message = items[1];
                    var date = items[2];

                    Add(user, message, date);

                }
            }
        }

        public void Add(string user, string message, string date)
        {
            server.Add(Tuple.Create(user, message, date));
        }
    }
}
