﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Chat
{
    public sealed class Application : IDisposable
    {
        private DataSerializer serializer = new DataSerializer();
        private readonly IConnection connection;
        private readonly IModel channel;
        private readonly IModel historyChannel;
        private Guid applicationToken = Guid.NewGuid();
        private readonly ServerLogs serverLogs = new ServerLogs();

        public Application()
        {

            var connectionFactory = new ConnectionFactory() { HostName = "localhost" };
            connection = connectionFactory.CreateConnection();
            channel = connection.CreateModel();

            channel.ExchangeDeclare("Chat", "fanout");

            var incomingMessages = channel.QueueDeclare();
            channel.QueueBind(incomingMessages.QueueName, "Chat", string.Empty);

            var consumer = new DelegateConsumer<ChatMessage>(OnMessage, serializer);
            channel.BasicConsume(incomingMessages.QueueName, true, consumer);

            historyChannel = connection.CreateModel();
            historyChannel.ExchangeDeclare("History", "fanout");
            var incomingHistoryMessages = channel.QueueDeclare();
            historyChannel.QueueBind(incomingHistoryMessages.QueueName, "History", string.Empty);
            var historyConsumer = new DelegateConsumer<InitialChatRequest>(OnHistoryRequest, serializer);
            historyChannel.BasicConsume(incomingHistoryMessages.QueueName, true, historyConsumer);

        }

        public void Run()
        {
            serverLogs.Load();

            Console.WriteLine("Press ENTER to save logs and exit");
            Console.ReadLine();

            serverLogs.Save();
        }

        private void OnMessage(ChatMessage message)
        {
            serverLogs.Add(message.Nick, message.Text, message.Date.ToString("yyyy-MM-dd HH:mm:ss"));
            serverLogs.Save();
        }

        private void OnHistoryRequest(InitialChatRequest message)
        {
        }

        public void Dispose()
        {
            channel.Dispose();
            connection.Dispose();
        }
    }

}
