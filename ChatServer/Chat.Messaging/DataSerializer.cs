﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Chat
{
    public sealed class DataSerializer
    {
        private object syncLock = new object();

        private Dictionary<Type, DataContractSerializer> serializers = new Dictionary<Type, DataContractSerializer>();

        public byte[] Serialize<T>(T message)
        {
            var serializer = GetSerializer(typeof(T));

            using (var stream = new MemoryStream())
            {
                serializer.WriteObject(stream, message);
                return stream.ToArray();
            }
        }

        public T Deserialize<T>(byte[] binaryContent)
        {
            var serializer = GetSerializer(typeof(T));

            using (var stream = new MemoryStream(binaryContent))
            {
                return (T)serializer.ReadObject(stream);
            }
        }

        private DataContractSerializer GetSerializer(Type type)
        {
            DataContractSerializer serializer;
            lock (syncLock)
            {
                if (!serializers.TryGetValue(type, out serializer))
                {
                    serializer = new DataContractSerializer(type);
                    serializers.Add(type, serializer);
                }
            }
            return serializer;
        }
    }
}
