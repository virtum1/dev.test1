﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat
{
    /// <summary>
    /// Implements <see cref="IBus"/> for RabbitMQ messaging platform.
    /// </summary>
    public sealed class RabbitBus : IBus
    {
        public Task<TResponse> Request<TRequest, TResponse>(TRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
