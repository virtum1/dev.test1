﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Chat
{
    public class DelegateConsumer<T> : IBasicConsumer
    {
        public event ConsumerCancelledEventHandler ConsumerCancelled;
        private readonly Action<T, string> onMessage;
        private DataSerializer serializer;

        /// <summary>
        /// Allows to handle incoming messages and reply topic.
        /// </summary>
        public DelegateConsumer(Action<T, string> onMessage, DataSerializer serializer)
        {
            this.onMessage = onMessage;
            this.serializer = serializer;
        }

        /// <summary>
        /// Allows to handle incoming messages.
        /// </summary>
        /// <param name="onMessage">Message handler.</param>
        /// <param name="serializer"></param>
        public DelegateConsumer(Action<T> onMessage, DataSerializer serializer)
            : this((o, s) => onMessage(o), serializer)
        {
        }

        public void HandleBasicCancel(string consumerTag)
        {
        }

        public void HandleBasicCancelOk(string consumerTag)
        {
        }

        public void HandleBasicConsumeOk(string consumerTag)
        {
        }

        public void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            onMessage(
                serializer.Deserialize<T>(body),
                properties.ReplyTo);

        }

        public void HandleModelShutdown(IModel model, ShutdownEventArgs reason)
        {
        }

        public IModel Model
        {
            get { throw new NotImplementedException(); }
        }
    }
}
