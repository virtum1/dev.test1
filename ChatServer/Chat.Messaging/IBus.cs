﻿namespace Chat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Allows to make request / response operation.
    /// </summary>
    public interface IBus
    {
        /// <summary>
        /// Sends a request and handle response.
        /// </summary>
        /// <param name="request">Request</param>
        /// <returns>Response</returns>
        Task<TResponse> Request<TRequest, TResponse>(TRequest request);
    }
}
