﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Eng
{
    public sealed class HomeViewModel : BaseViewModel, IPageViewModel
    {
        public string Name
        {
            get { return "Strona głowna"; }
        }
    }
}
