﻿using Eng.Database;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;

namespace Eng.Lesson
{
    public sealed class LessonViewModel : BaseViewModel, IPageViewModel, INotifyPropertyChanged
    {
        public ObservableCollection<LessonDataModel> Lessons { get; set; }
        public CollectionViewSource CVS { get; set; }
        public ICommand LoadedCommand { get; private set; }
        //public ICommand RefreshDataButton { get; set; }

        public LessonViewModel()
        {
            LoadedCommand = new DelegateCommand<object>(o => OnLoaded());
            // RefreshDataButton = new DelegateCommand<object>( o => RefreshLessons());

            Lessons = new ObservableCollection<LessonDataModel>();
            this.CVS = new CollectionViewSource();
            this.CVS.Source = this.Lessons;

            CVS.Filter += CVS_Filter;
        }

        private void OnGotFocus()
        {
            using (var ctx = new Context())
            {
                ChooseLesson = ctx.GetLessons().ToList();
                PropertyChanged.Raise(this, "ChooseLesson");
            }
        }

        private void RefreshLessons()
        {
            Lessons.Clear();

            using (var ctx = new Context())
            {
                var values = from list in ctx.Lesson
                             where list.LessonName == ChoosedLessonName
                             select list;

                foreach (var item in values)
                {
                    Lessons.Add(item);
                }
            }
        }

        private void OnLoaded()
        {
            //Zakomentowane gdyż nie chce by dataGrid pokazywał wszystkie lekcje, pojawią się dopiero po wyborze lessoName z
            //comboBoxa.
            //Lessons.Clear();

            //using (var ctx = new Context())
            //{
            //    var values = from list in ctx.Lesson
            //                 // where list.LessonName == ChoosedLessonName
            //                 select list;

            //    foreach (var item in values)
            //    {
            //        Lessons.Add(item);
            //    }
            //}
            //Dodałem w metodzie notyfikacje ze property sie zmieniło, jednak activate command nie bangla
            OnGotFocus();
        }

        private void CVS_Filter(object sender, FilterEventArgs e)
        {
            var typedItem = e.Item as LessonDataModel;
            if (typedItem.LessonName != ChoosedLessonName)
            {
                e.Accepted = false;
            }

            e.Accepted = true;
        }

        public string Name
        {
            get { return "Lekcje"; }
        }

        public List<string> ChooseLesson { get; set; }

        private string chooseLessonName;
        public string ChoosedLessonName
        {
            get { return chooseLessonName; }
            set
            {
                chooseLessonName = value;
                RefreshLessons();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
