﻿using Eng.Database;
using Eng.Shell;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Eng.Mix
{
    public sealed class MixViewModel : BaseViewModel, IPageViewModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand LoadedCommand { get; private set; }
        public ICommand AddLesson { get; set; }
        public ICommand NextView { get; set; }
        private List<LessonDataModel> listToShow = new List<LessonDataModel>();
        private ShellViewModel svm;

        public MixViewModel(ShellViewModel svm)
        {
            this.svm = svm;
            LoadedCommand = new DelegateCommand<object>(o => OnGotFocus());
            AddLesson = new DelegateCommand<object>(o => OnPressAddLesson());
            NextView = new DelegateCommand<object>(o=> OnPressNextView());
        }

        private void OnPressNextView()
        {
            var secondView = new MixSecondViewModel(svm, listToShow);
            svm.ChangeViewModel(secondView);
        }

        private void OnPressAddLesson()
        {
            using (var ctx = new Context())
            {
                var lessonList = from list in ctx.Lesson
                                 where list.LessonName == ChoosedLessonName
                                 select list;

                foreach (var item in lessonList)
                {
                    listToShow.Add(item);
                }
            }

            ChosenLessonName += "- " + ChoosedLessonName + "\n";
            PropertyChanged.Raise(this, "ChosenLessonName");
        }

        public string ChosenLessonName { get; set; }

        private void OnGotFocus()
        {
            using (var ctx = new Context())
            {
                ChooseLesson = ctx.GetLessons().ToList();
                PropertyChanged.Raise(this, "ChooseLesson");
            }
        }

        public List<string> ChooseLesson { get; set; }
        public string ChoosedLessonName
        { get; set; }

        private MixSecondViewModel goToSecondView;
        public MixSecondViewModel GoToSecondView
        {
            get { return goToSecondView; }
            set
            {
                goToSecondView = value;
                PropertyChanged.Raise(this, "GoToSecondView");
            }
        }

        public string Name
        {
            get { return "MIX"; }
        }
    }
}
