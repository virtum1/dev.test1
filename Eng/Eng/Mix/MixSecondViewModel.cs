﻿using Eng.Database;
using Eng.Shell;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Eng.Mix
{
    public sealed class MixSecondViewModel : BaseViewModel, IPageViewModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand FirstView { get; set; }
        public ICommand DrawButton { get; set; }
        public ICommand CorrectButton { get; set; }
        public ICommand HintButton { get; set; }
        private List<LessonDataModel> listToShow;
        private List<string> tempList;
        private ShellViewModel svm;
        private int counter = 0;

        public MixSecondViewModel(ShellViewModel svm, List<LessonDataModel> listToShow)
        {
            this.svm = svm;
            this.listToShow = listToShow;

            var lessonValues = from lista in listToShow
                               select lista.PolishDescription;

            tempList = lessonValues.ToList<string>();
            var numbersLeft = tempList.Count.ToString();
            WordsLeft = "Pozostało:\n" + numbersLeft;

            CorrectButton = new DelegateCommand<object>(OnPressCorrectButton);
            FirstView = new DelegateCommand<object>(o => OnPressFirstView());
            DrawButton = new DelegateCommand<object>(OnPressDrawButton);
            HintButton = new DelegateCommand<object>(o => OnPressHintButton());
        }

        private void OnPressHintButton()
        {
            try
            {
                using (var ctx = new Context())
                {
                    var check = (from lista in listToShow
                                 where lista.PolishDescription == DrawnValues
                                 select lista.EnglishDescription)
                                    .First();

                    HintLabel = check;
                    PropertyChanged.Raise(this, "HintLabel");
                }
            }
            catch (InvalidOperationException)
            {
                HintLabel = "Brak wartości!";
                PropertyChanged.Raise(this, "HintLabel");
            }
        }

        private void OnPressCorrectButton(object obj)
        {
            try
            {
                var check = (from lista in listToShow
                             where lista.PolishDescription == DrawnValues
                             select lista.EnglishDescription)
                            .First();

                if (valueToCheck == check)
                {
                    CheckedValue = "Dobrze!";
                    counter++;
                }
                else
                {
                    CheckedValue = "Źle!";
                    counter--;
                }
            }
            catch(InvalidOperationException)
            {
                CheckedValue = "Brak wartości!";
            }


                //CheckedValue = valueToCheck == check
                //    ? "Dobrze" + counter++
                //    : "Źle" + counter--;
        }

        private void OnPressDrawButton(object obj)
        {
            try
            {
                HintLabel = string.Empty;
                ValueToCheck = string.Empty;
                CheckedValue = string.Empty;
                EndOfValues = string.Empty;
                var random = new Random();
                var r = random.Next(tempList.Count());
                var drawnValue = tempList[r];
                tempList.RemoveAt(r);
                var numbersLeft = tempList.Count.ToString();
                WordsLeft = "Pozostało:\n" + numbersLeft;

                DrawnValues = drawnValue;
            }
            catch (ArgumentOutOfRangeException)
            {
                DrawnValues = string.Empty;
                EndOfValues = "Brak wartości!\nTwój wynik to: " + counter;
            }
        }

        public string HintLabel { get; set; }

        private string wordsLeft;
        public string WordsLeft
        {
            get { return wordsLeft; }
            set
            {
                wordsLeft = value;
                PropertyChanged.Raise(this, "WordsLeft");
            }
        }

        private string drawnValues;
        public string DrawnValues
        {
            get { return drawnValues; }
            set
            {
                drawnValues = value;
                PropertyChanged.Raise(this, "DrawnValues");
            }
        }

        private string checkedValue;
        public string CheckedValue
        {
            get { return checkedValue; }
            set
            {
                checkedValue = value;
                PropertyChanged.Raise(this, "CheckedValue");
            }
        }

        private string endOfValues;
        public string EndOfValues
        {
            get { return endOfValues; }
            set
            {
                endOfValues = value;
                PropertyChanged.Raise(this, "EndOfValues");
            }
        }

        private string valueToCheck;
        public string ValueToCheck
        {
            get { return valueToCheck; }
            set
            {
                valueToCheck = value;
                PropertyChanged.Raise(this, "ValueToCheck");
            }
        }

        private void OnPressFirstView()
        {
            var firstView = new MixViewModel(svm);
            svm.ChangeViewModel(firstView);
        }

        private MixViewModel backToFirstView;
        public MixViewModel BackToFirstView
        {
            get { return backToFirstView; }
            set
            {
                backToFirstView = value;
                PropertyChanged.Raise(this, "BackToFirstView");
            }
        }

        public string Name
        {
            get { return ""; }
        }


    }
}
