﻿using Eng.Database;
using Eng.Shell;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Eng.StartViews
{
    public sealed class StartSecondViewModel : BaseViewModel, IPageViewModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand FirstViewButton { get; set; }
        public ICommand DrawButton { get; set; }
        public ICommand CorrectButton { get; set; }
        public ICommand HintButton { get; set; }
        private ShellViewModel svm;
        private string lessonName;
        private List<string> tempList;
        private int counter = 0;

        public StartSecondViewModel(ShellViewModel svm, string lessonName)
        {
            this.lessonName = lessonName;
            this.svm = svm;
            using (var ctx = new Context())
            {
                var lessonValues = from lista in ctx.Lesson
                                   where lista.LessonName == lessonName
                                   select lista.PolishDescription;

                tempList = lessonValues.ToList<string>();
                var numbersLeft = tempList.Count.ToString();
                WordsLeft = "Pozostało:\n" + numbersLeft;
            }

            FirstViewButton = new DelegateCommand<object>(OnPressFirstViewButton);
            DrawButton = new DelegateCommand<object>(OnPressDrawButton);
            CorrectButton = new DelegateCommand<object>(OnPressCorrectButton);
            HintButton = new DelegateCommand<object>(o => OnPressHintButton());

        }

        public string HintLabel { get; set; }

        private void OnPressHintButton()
        {
            try
            {
                using (var ctx = new Context())
                {
                    var check = (from lista in ctx.Lesson
                                 where lista.PolishDescription == DrawnValues && lista.LessonName == lessonName
                                 select lista.EnglishDescription)
                                    .First();

                    HintLabel = check;
                    PropertyChanged.Raise(this, "HintLabel");
                }
            }
            catch (InvalidOperationException)
            {
                HintLabel = "Brak wartości!";
                PropertyChanged.Raise(this, "HintLabel");
            }
        }

        private void OnPressCorrectButton(object obj)
        {
            try
            {
                using (var ctx = new Context())
                {
                    var check = (from lista in ctx.Lesson
                                 where lista.PolishDescription == DrawnValues && lista.LessonName == lessonName
                                 select lista.EnglishDescription)
                                .First();

                    if (valueToCheck == check)
                    {
                        CheckedValue = "Dobrze!";
                        counter++;
                    }
                    else
                    {
                        CheckedValue = "Źle!";
                        counter--;
                    }
                }
            }

            catch (InvalidOperationException)
            {
                CheckedValue = "Brak wartości!";
            }


            //CheckedValue = valueToCheck == check
            //    ? "Dobrze" + counter++
            //    : "Źle" + counter--;

        }

        private void OnPressDrawButton(object obj)
        {
            try
            {
                HintLabel = string.Empty;
                ValueToCheck = string.Empty;
                CheckedValue = string.Empty;
                EndOfValues = string.Empty;
                var random = new Random();
                var r = random.Next(tempList.Count());
                var drawnValue = tempList[r];
                tempList.RemoveAt(r);
                var numbersLeft = tempList.Count.ToString();
                WordsLeft = "Pozostało:\n" + numbersLeft;

                //Dlaczego jak dałem tu prywatna zmienna to nie poszło ?
                DrawnValues = drawnValue;
            }
            catch (ArgumentOutOfRangeException)
            {
                DrawnValues = string.Empty;
                EndOfValues = "Brak wartości!\nTwój wynik to: " + counter;
            }

        }

        private void OnPressFirstViewButton(object obj)
        {
            var firstView = new StartViewModel(svm);
            svm.ChangeViewModel(firstView);
        }

        private StartViewModel firstView;
        public StartViewModel FirstView
        {
            get { return firstView; }
            set
            {
                firstView = value;
                PropertyChanged.Raise(this, "FirstView");
            }
        }

        private string drawnValues;
        public string DrawnValues
        {
            get { return drawnValues; }
            set
            {
                drawnValues = value;
                PropertyChanged.Raise(this, "DrawnValues");
            }
        }

        public string Name
        {
            get { return "SecondView"; }
        }

        private string endOfValues;
        public string EndOfValues
        {
            get { return endOfValues; }
            set
            {
                endOfValues = value;
                PropertyChanged.Raise(this, "EndOfValues");
            }
        }

        private string valueToCheck;
        public string ValueToCheck
        {
            get { return valueToCheck; }
            set
            {
                valueToCheck = value;
                PropertyChanged.Raise(this, "ValueToCheck");
            }
        }

        private string checkedValue;
        public string CheckedValue
        {
            get { return checkedValue; }
            set
            {
                checkedValue = value;
                PropertyChanged.Raise(this, "CheckedValue");
            }
        }

        private string wordsLeft;
        public string WordsLeft
        {
            get { return wordsLeft; }
            set
            {
                wordsLeft = value;
                PropertyChanged.Raise(this, "WordsLeft");
            }
        }
    }
}
