﻿using Eng.Database;
using Eng.Shell;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Eng.StartViews
{
    public sealed class StartViewModel : BaseViewModel ,IPageViewModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand SecondViewButton {get; set; }
        public ICommand LoadedCommand { get; private set; }

        private ShellViewModel svm;

        public StartViewModel(ShellViewModel svm)
        {
            LoadedCommand = new DelegateCommand<object>(o => OnGotFocus());
            this.svm = svm;
            SecondViewButton = new DelegateCommand<object>(OnPressSecondViewButton);

        }

        private void OnGotFocus()
        {
            using (var ctx = new Context())
            {
                ChooseLesson = ctx.GetLessons().ToList();
                PropertyChanged.Raise(this, "ChooseLesson");
            }
        }

        private void OnPressSecondViewButton(object arg)
        {
            var secondView = new StartSecondViewModel(svm, ChoosedLessonName);
            svm.ChangeViewModel(secondView);
        }

        private StartSecondViewModel secondView;
        public StartSecondViewModel SecondView
        {
            get { return secondView; }
            set
            {
                secondView = value;
                PropertyChanged.Raise(this, "SecondView");
            }
        }

        public List<string> ChooseLesson { get; set; }
        public string ChoosedLessonName
        { get; set; }

        public string Name
        {
            get { return "START"; }
        }
    }
}
