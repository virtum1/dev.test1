﻿using Castle.Core;
using Eng.Lessons;
using Eng.Lesson;
using Eng.StartViews;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Eng.Mix;

namespace Eng.Shell
{
    public sealed class ShellViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private List<IPageViewModel> pageViewModels;

        public ShellViewModel()
        {
            // Add available pages
            PageViewModels.Add(new HomeViewModel());
            PageViewModels.Add(new AddLessonViewModel(this));
            PageViewModels.Add(new StartViewModel(this));
            PageViewModels.Add(new LessonViewModel());
            PageViewModels.Add(new MixViewModel(this));

            // Set starting page
            CurrentPageViewModel = PageViewModels[0];

            ChangePageCommand = new DelegateCommand<object>(
                        p => ChangeViewModel((IPageViewModel)p),
                        p => p is IPageViewModel);
        }


        public ICommand ChangePageCommand { get; private set; }

        public List<IPageViewModel> PageViewModels
        {
            get
            {
                if (pageViewModels == null)
                    pageViewModels = new List<IPageViewModel>();

                return pageViewModels;
            }
        }

        public IPageViewModel CurrentPageViewModel { get; private set; }

        public void ChangeViewModel(IPageViewModel viewModel)
        {
            CurrentPageViewModel = viewModel;

            PropertyChanged.Raise(this, "CurrentPageViewModel");
        }
    }
}
