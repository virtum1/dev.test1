﻿using System;
using System.Diagnostics;
/// <summary>
/// A command whose sole purpose is to relay its functionality to other
/// objects by invoking delegates. The default return value for the
/// CanExecute method is 'true'.
/// </summary>
using System.Windows.Input;
public sealed class DelegateCommand<T> : ICommand
{
    private readonly Action<T> execute;
    private readonly Predicate<T> canExecute;

    public DelegateCommand(Action<T> execute)
        : this(execute, null)
    {
    }

    public DelegateCommand(Action<T> execute, Predicate<T> canExecute)
    {
        this.execute = execute;
        this.canExecute = canExecute;
    }

    public bool CanExecute(object parameter)
    {
        if (canExecute == null)
        {
            return true;
        }

        return canExecute((T)parameter);
    }

    public event EventHandler CanExecuteChanged;

    public void Execute(object parameter)
    {
        execute((T)parameter);
    }

    public void RaiseCanExecuteChanged()
    {
        if (CanExecuteChanged == null)
        {
            return;
        }
        CanExecuteChanged(this, EventArgs.Empty);
    }
}