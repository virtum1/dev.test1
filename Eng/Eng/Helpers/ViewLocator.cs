﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Eng
{
    public sealed class ViewLocator : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;

            var vmName = value.GetType().FullName;
            Debug.Assert(vmName.EndsWith("Model"));

            var viewName = vmName.Split(new [] {"Model"}, StringSplitOptions.None)[0];
            var viewType = Type.GetType(viewName);
            return Activator.CreateInstance(viewType);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new InvalidOperationException();
        }
    }
}
