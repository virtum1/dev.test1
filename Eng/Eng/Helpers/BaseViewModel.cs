﻿namespace Eng
{
    /// <summary>
    /// base class for all ViewModels 
    /// need to be used - WPF DataTemplate can't use DataTemplate only for 
    /// types, not for interfaces.
    /// </summary>
    public abstract class BaseViewModel
    {
    }
}
