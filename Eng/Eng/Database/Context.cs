﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eng.Database
{
    public class Context : DbContext
    {
        public Context()
            : base()
        {

        }

        public DbSet<LessonDataModel> Lesson { get; set; }
    }
}
