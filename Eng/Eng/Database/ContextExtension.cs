﻿using System.Collections.Generic;
using System.Linq;

namespace Eng.Database
{
    public static class ContextExtension
    {
        public static IEnumerable<string> GetLessons(this Context context)
        {
            return (context.Lesson
                .GroupBy(o => o.LessonName)
                .Select(o => o.Key))
                .OrderBy(ContextHelper.Convert);
        }
    }
}