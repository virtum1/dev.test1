﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eng.Database
{
    [Table("Lessons")]
    public sealed class LessonDataModel
    {
        public int Id { get; set; }
        public string LessonName { get; set; }
        public string EnglishDescription { get; set; }
        public string PolishDescription { get; set; }
    }
}
