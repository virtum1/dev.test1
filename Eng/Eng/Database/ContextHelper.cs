﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eng.Database
{
    public static class ContextHelper
    {
        public static int Convert(string lessonName)
        {
            var s = lessonName.Substring(7);
            return Int32.Parse(s);
        }
    }
}
