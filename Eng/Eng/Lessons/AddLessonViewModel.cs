﻿using Eng.Database;
using Eng.Shell;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Eng.Lessons
{
    public sealed class AddLessonViewModel : BaseViewModel, IPageViewModel, INotifyPropertyChanged
    {
        public ICommand SecondViewButton { get; set; }
        public ICommand CreateNewLessonButton { get; set; }
        public ICommand ChooseExistingLessonButton { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private ShellViewModel svm;


        public AddLessonViewModel(ShellViewModel svm)
        {
            this.svm = svm;
            SecondViewButton = new DelegateCommand<object>(OnPressSecondViewButton);
            CreateNewLessonButton = new DelegateCommand<object>(OnPressCreateNewLessonButton);
            ChooseExistingLessonButton = new DelegateCommand<object>(OnPressChooseExistingLessonButton);

            using (var ctx = new Context())
            {
                ChooseLesson = ctx.GetLessons().ToList();
            }

        }

        private void OnPressChooseExistingLessonButton(object arg)
        {
            var secondView = new AddLessonSecondViewModel(svm, ChoosedLessonName);
            svm.ChangeViewModel(secondView);
        }

        private void OnPressCreateNewLessonButton(object arg)
        {
                var secondView = new AddLessonSecondViewModel(svm, lessonName);
                svm.ChangeViewModel(secondView);
        }

        private string lessonName;
        public string LessonName
        {
            get
            {
                return lessonName;
            }
            set
            {
                lessonName = value;
                PropertyChanged.Raise(this, "LessonName");
            }
        }

        public void OnPressSecondViewButton(object arg)
        {
            //var secondView = new AddLessonSecondViewModel(svm);
            //svm.ChangeViewModel(secondView);
        }

        private AddLessonSecondViewModel goToSecondView;
        public AddLessonSecondViewModel GoToSecondView
        {
            get
            {
                return goToSecondView;
            }
            set
            {
                goToSecondView = value;
                PropertyChanged.Raise(this, "GoToSecondView");
            }
        }

        public string Name
        {
            get { return "Dodaj lekcję"; }
        }

        public List<string> ChooseLesson { get; set; }
        public string ChoosedLessonName 
        { get; set; }
    }
}
