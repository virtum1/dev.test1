﻿using Eng.Database;
using Eng.Shell;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Eng.Lessons
{
    public sealed class AddLessonSecondViewModel : BaseViewModel, IPageViewModel, INotifyPropertyChanged
    {
        public ICommand FirstViewButton { get; set; }
        public ICommand AddLessonButton { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private ShellViewModel svm;
        private string lessonName;

        public AddLessonSecondViewModel(ShellViewModel svm, string lessonName)
        {
            this.lessonName = lessonName;
            this.svm = svm;
            FirstViewButton = new DelegateCommand<object>(OnPressFirstViewButton);
            AddLessonButton = new DelegateCommand<object>(OnPressAddLessonButton);

            using (var ctx = new Context())
            {
                var lessonsNumber = from list in ctx.Lesson
                                    where list.LessonName == lessonName
                                    select list;

                var wordsNumber = lessonsNumber.ToList();
                WordsNumber = wordsNumber.Count.ToString();
                PropertyChanged.Raise(this, "WordsNumber");
            }
        }

        private void OnPressAddLessonButton(object arg)
        {
            using (var ctx = new Context())
            {
                var newLesson = new LessonDataModel() { LessonName = lessonName, EnglishDescription = englishDescription, PolishDescription = polishDescription };
                ctx.Lesson.Add(newLesson);
                ctx.SaveChanges();

                var lessonsNumber = from list in ctx.Lesson
                                    where list.LessonName == lessonName
                                    select list;

                var wordsNumber = lessonsNumber.ToList();
                WordsNumber = wordsNumber.Count.ToString();
                PropertyChanged.Raise(this, "WordsNumber");

            }

            LessonAdded = "Dodano: " + EnglishDescription + " - " + PolishDescription;
            EnglishDescription = string.Empty;
            PolishDescription = string.Empty;
        }

        public string WordsNumber { get; set; }

        private string englishDescription;
        public string EnglishDescription
        {
            get { return englishDescription; }
            set
            {
                englishDescription = value;
                PropertyChanged.Raise(this, "EnglishDescription");
            }
        }

        private string lessonAdded;
        public string LessonAdded
        {
            get { return lessonAdded; }
            set
            {
                lessonAdded = value;
                PropertyChanged.Raise(this, "LessonAdded");
            }
        }

        private string polishDescription;
        public string PolishDescription
        {
            get { return polishDescription; }
            set
            {
                polishDescription = value;
                PropertyChanged.Raise(this, "PolishDescription");
            }
        }

        public void OnPressFirstViewButton(object arg)
        {
            var firstView = new AddLessonViewModel(svm);
            svm.ChangeViewModel(firstView);
        }

        private AddLessonViewModel backToFirstView;
        public AddLessonViewModel BackToFirstView
        {
            get
            {
                return backToFirstView;
            }
            set
            {
                backToFirstView = value;
                PropertyChanged.Raise(this, "BackToFirstView");
            }
        }

        public string Name
        {
            get { return ""; }
        }

    }
}
