﻿using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using Eng.Shell;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Eng
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        ShellViewModel shellViewModel;  
        protected override void OnStartup(StartupEventArgs e)
        {
            var container = new WindsorContainer();
            container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel));
            container.Install(new IocInstaller());
            shellViewModel = container.Resolve<ShellViewModel>();

            base.OnStartup(e);

            var shellView = new ShellView();
            shellView.DataContext = shellViewModel;
            base.MainWindow = shellView;
            shellView.Show();
        }
    }
}
