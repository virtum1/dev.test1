﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eng.Database
{
    [TestFixture]
    public class ContextHelperTests
    {
        [Test]
        public void ShouldConvertToNumber()
        {
            var i1 = ContextHelper.Convert("Lesson 1");
            var i2 =ContextHelper.Convert("Lesson 2");

            Assert.That(i1, Is.LessThan(i2));
        }
    }
}
