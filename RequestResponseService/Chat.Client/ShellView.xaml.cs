﻿using Castle.Core;
using System.Windows;
using System.Windows.Controls;

namespace Chat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ShellView : Window, IInitializable
    {
        public ShellViewModel DataContext
        {
            set { base.DataContext = value; }
        }

        public ShellView()
        {
            InitializeComponent();
        }

        public void Initialize()
        {
        }
    }
}
