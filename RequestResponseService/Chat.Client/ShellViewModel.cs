﻿using Gadulec;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Reactive.Linq;
using Castle.Core;
using Messaging;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Threading;

namespace Chat
{
    public sealed class ShellViewModel : INotifyPropertyChanged, IInitializable
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand OnInputEnterCommand { get; set; }
        public ICommand OnNickEnterCommand { get; set; }
        private readonly IBus bus;
        public IBus Bus { get; set; }
        private Guid applicationToken = Guid.NewGuid();

        private string inputText;
        public string InputText
        {
            get { return inputText; }
            set
            {
                inputText = value;
                if (PropertyChanged == null) return;
                PropertyChanged(this, new PropertyChangedEventArgs("InputText"));
            }
        }
        public ObservableCollection<string> MessagesHistory { get; set; }
        


        private string nick;
        public string Nick
        {
            get { return nick; }
            set
            {
                nick = value;
                if (PropertyChanged == null) return;
                PropertyChanged(this, new PropertyChangedEventArgs("Nick"));
            }
        }

        private ISynchronizationContext current;
        public ShellViewModel(IBus bus, ISynchronizationContext asyncContext)
        {
            current = asyncContext;
            this.bus = bus;

            bus
            .On<NotifyMessage>()
            .Subscribe(OnNotify);

            OnNickEnterCommand = new DelegateCommand(OnNickKeyDown);
            OnInputEnterCommand = new DelegateCommand(OnKeyDown);
            MessagesHistory = new ObservableCollection<string>();
            
        }

        private void OnNotify(NotifyMessage message)
        {
            current.Post(() => MessagesHistory.Add(message.Description));
        }

        public void OnKeyDown(object arg)
        {
            SendMessage(nick, InputText);
            InputText = null;
        }

        public void OnNickKeyDown(object arg)
        {
            Nick = nick;
        }

        private void SendMessage(string user, string message)
        {
            if (message == "kill")
            {
                bus.Publish(new KillMessage { });
            }
            else
            {
                bus.Publish(new NotifyMessage { Description = DateTime.Now + " - " + user + " - " + message });
            }
            var msg = new Message { Nick = user, Text = message, ApplicationToken = applicationToken, Date = DateTime.Now };
            bus.Request<Message, Message>(msg);

            //Console.WriteLine("{2} [{0}] - {1}", response.Result.Nick, response.Result.Text, response.Result.Date.ToString("yyyy-MM-dd HH:mm:ss"));

        }

        public void Initialize()
        {
        }
    }
}
