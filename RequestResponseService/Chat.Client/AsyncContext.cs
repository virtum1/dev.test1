﻿using Castle.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Chat
{
    public class AsyncContext : ISynchronizationContext, IInitializable
    {
        private SynchronizationContext guiContext;
        public void Post(Action action)
        {
            guiContext.Post(o => action(), null);
        }

        public void Initialize()
        {
            guiContext = SynchronizationContext.Current;
        }
    }
}
