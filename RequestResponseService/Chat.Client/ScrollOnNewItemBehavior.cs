﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace Chat
{
    // http://stackoverflow.com/questions/2006729/how-can-i-have-a-listbox-auto-scroll-when-a-new-item-is-added
    public sealed class ScrollOnNewItemBehavior : Behavior<ItemsControl>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            base.AssociatedObject.Loaded += AssociatedObject_Loaded;
        }

        void AssociatedObject_Loaded(object sender, RoutedEventArgs e)
        {
            var source = AssociatedObject.ItemsSource as INotifyCollectionChanged;
            if (source == null) return;

            source.CollectionChanged += sourceCollectionChanged;
        }

        void sourceCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                AssociatedObject.UpdateLayout();
                var lastItem = AssociatedObject.Items.OfType<object>().Last();
                var frameworkElement = AssociatedObject.ItemContainerGenerator.ContainerFromItem(lastItem) as FrameworkElement;
                if (frameworkElement == null) return;
                frameworkElement.BringIntoView();
            }
        }
    }
}
