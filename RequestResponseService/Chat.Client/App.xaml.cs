﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Chat.Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        [STAThread]
        public static void Main()
        {
            var app = new App();
            app.Run();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            var container = new WindsorContainer();
            container.Register(
                Component.For<ShellView>(),
                Component.For<ShellViewModel>(),
                Component.For<ISynchronizationContext>().ImplementedBy<AsyncContext>(),
                Component.For<Gadulec.IBus>().ImplementedBy<Gadulec.BusProvider>());
            var view = container.Resolve<ShellView>();
            
            this.MainWindow = view;
            view.Show();
            base.OnStartup(e);
        }
    }
}
