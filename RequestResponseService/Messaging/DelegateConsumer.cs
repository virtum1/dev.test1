﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Gadulec
{
    public class DelegateConsumer : IBasicConsumer
    {
        public event ConsumerCancelledEventHandler ConsumerCancelled;
        private readonly Action<object> onMessage;
        private DataSerializer serializer;

        public DelegateConsumer(Action<object> onMessage, DataSerializer serializer)
        {
            this.onMessage = onMessage;
            this.serializer = serializer;
        }

        public void HandleBasicCancel(string consumerTag)
        {
        }

        public void HandleBasicCancelOk(string consumerTag)
        {
        }

        public void HandleBasicConsumeOk(string consumerTag)
        {
        }

        public void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            var type = Type.GetType(properties.ContentType);
            onMessage(
                serializer.Deserialize(body, type));

        }

        public void HandleModelShutdown(IModel model, ShutdownEventArgs reason)
        {
        }

        public IModel Model
        {
            get { throw new NotImplementedException(); }
        }

    }
}
