﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Gadulec
{
    public sealed class DataSerializer
    {
        private object syncLock = new object();

        public byte[] Serialize(object message)
        {
            var data = JsonConvert.SerializeObject(message);
            return Encoding.UTF8.GetBytes(data);
        }

        public object Deserialize(byte[] binaryContent, Type instanceType)
        {
            var data = Encoding.UTF8.GetString(binaryContent);
            return JsonConvert.DeserializeObject(data, instanceType);
        }
    }
}
