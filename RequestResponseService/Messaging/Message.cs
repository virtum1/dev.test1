﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gadulec
{
    public class Message
    {
        public string Text { get; set; }
        public string Nick { get; set; }
        public Guid ApplicationToken { get; set; }
        public DateTime Date { get; set; }
    }
}
