﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messaging
{
    public sealed class NotifyMessage
    {
        public string Description { get; set; }
    }

    public sealed class KillMessage
    {
    }
}
