﻿using Messaging;
using RabbitMQ.Client;
using RabbitMQ.Client.Framing.v0_9_1;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Gadulec
{
    public sealed class BusProvider : IBus, IDisposable
    {
        private DataSerializer serializer = new DataSerializer();
        private readonly IConnection connection;
        private readonly IModel chatModel;
        private readonly IModel notificationChannel;

        private string clientRoutingKey = Guid.NewGuid().ToString();

        public BusProvider()
        {
            var connectionFactory = new ConnectionFactory() { HostName = "localhost" };
            connection = connectionFactory.CreateConnection();

            chatModel = connection.CreateModel();
            chatModel.ExchangeDeclare("ggadulec", "direct", false, true, null);

            var incomingMessages = chatModel.QueueDeclare();
            chatModel.QueueBind(incomingMessages.QueueName, "ggadulec", clientRoutingKey);
            var consumer = new DelegateConsumer<Message>(OnMessage, serializer);
            chatModel.BasicConsume(incomingMessages.QueueName, true, consumer);

            notificationChannel = connection.CreateModel();
            notificationChannel.ExchangeDeclare("BusProvider.Notifications", "fanout");
            var notificationsQueue = notificationChannel.QueueDeclare();
            notificationChannel.QueueBind(notificationsQueue.QueueName, "BusProvider.Notifications", string.Empty);
            notificationChannel.BasicConsume(notificationsQueue.QueueName, true, new DelegateConsumer(OnNotify, serializer));
        }

        private readonly Dictionary<Type, object> subjects = new Dictionary<Type, object>();
        public IObservable<TNotification> On<TNotification>()
        {
            var notificationType = typeof(TNotification);
            object subject;
            if (!subjects.TryGetValue(notificationType, out subject))
            {
                subject = new Subject<TNotification>();
                subjects.Add(notificationType, subject);
            }

            return (IObservable<TNotification>)subject;
        }

        private void OnNotify(object message)
        {
            var notificationType = message.GetType();
            dynamic subject;
            if (!subjects.TryGetValue(notificationType, out subject)) return;

            var subjectGenericType = typeof(Subject<>);
            var subjectType = subjectGenericType.MakeGenericType(notificationType);
            var onNextMethod = subjectType.GetMethod("OnNext", BindingFlags.Instance | BindingFlags.Public);
            onNextMethod.Invoke(subject, new [] { message });

        }

        private ConcurrentDictionary<string, object> waitingRequests = new ConcurrentDictionary<string, object>();
        private static int correlationGenerator = 0;
        public Task<TResponse> Request<TRequest, TResponse>(TRequest request)
        {
            var tcs = new TaskCompletionSource<TResponse>();
            var correlationId = Interlocked.Increment(ref correlationGenerator).ToString();

            chatModel.BasicPublish("ggadulec", AppConstants.ServerRoutingKey,
                    new BasicProperties { ReplyTo = clientRoutingKey, CorrelationId = correlationId }, serializer.Serialize(request));

            waitingRequests.TryAdd(correlationId, tcs);
            return tcs.Task;
        }

        private void OnMessage(Message response, MessageContext context)
        {
            var correlctionId = context.CorrelationId;
            dynamic tcs;
            waitingRequests.TryRemove(correlctionId, out tcs);
            tcs.SetResult(response);
        }

        public void Dispose()
        {
            notificationChannel.Dispose();
            chatModel.Dispose();
            connection.Dispose();
        }

        public void Publish<TNotification>(TNotification message)
        {
            chatModel.BasicPublish("BusProvider.Notifications",
                string.Empty,
                new BasicProperties
                {
                    ContentType = typeof(TNotification).FullName
                },
                serializer.Serialize(message));
        }
    }
}
