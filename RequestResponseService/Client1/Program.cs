﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Gadulec
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Podaj swój nick");
            var userName = Console.ReadLine();
            Console.WriteLine("Witaj {0}, możesz rozpocząć czatowanie!\nNapisz to co chcesz wysłać", userName);

            using (var bus = new BusProvider())
            {
                var app = new Application(userName, bus);
                app.Run();
            }
        }
    }
}
