﻿namespace Gadulec
{
    using Messaging;
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// Provider some basic operations for messaging, like: Request / Response, Publishing.
    /// </summary>
    public interface IBus
    {

        Task<TResponse> Request<TRequest, TResponse>(TRequest request);


        IObservable<TNotification> On<TNotification>();


        void Publish<TNotification>(TNotification message);
    }
}
