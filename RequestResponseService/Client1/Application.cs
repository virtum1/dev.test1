﻿using Messaging;
using System;

namespace Gadulec
{
    public sealed class Application
    {
        private Guid applicationToken = Guid.NewGuid();
        private IBus bus;
        private readonly string userNick;
        public Application(string userNick, IBus bus)
        {
            this.userNick = userNick;
            this.bus = bus;
        }

        public void Run()
        {
            bus
                .On<NotifyMessage>()
                .Subscribe(OnNotify);

            bus
                .On<KillMessage>()
                .Subscribe(OnKillMessage);

            while (true)
            {
                var message = Console.ReadLine();
                SendMessage(userNick, message);
            }
        }

        private void OnNotify(NotifyMessage message)
        {
            Console.WriteLine(message.Description);
        }

        private void OnKillMessage(KillMessage message)
        {
            Environment.Exit(0);
        }

        private void SendMessage(string user, string message)
        {
            if (message == "kill")
            {
                bus.Publish(new KillMessage { });
            }
            else
            {
                bus.Publish(new NotifyMessage { Description = user + " " + message });
            }
            var msg = new Message { Nick = user, Text = message, ApplicationToken = applicationToken, Date = DateTime.Now };
            var response = bus.Request<Message, Message>(msg);

            response.ContinueWith(t =>
            {
                Console.WriteLine("{2} [{0}] - {1}", t.Result.Nick, t.Result.Text, t.Result.Date.ToString("yyyy-MM-dd HH:mm:ss"));
            });
        }

        public void Dispose()
        {
        }
    }
}
