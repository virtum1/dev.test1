﻿using Chat;
using Gadulec;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatClientTest
{
    [TestFixture]
    public class Class1
    {
        [Test]
        public void ShouldClearInputAfterEnter()
        {
            var viewModel = new ShellViewModel();
            viewModel.InputText = "abc";
            viewModel.OnInputEnterCommand.Execute(null);
            Assert.That(viewModel.InputText, Is.Null);
        }

        [Test]
        public void ShouldShowMessageAfterEnter()
        {
            var bus = Substitute.For<IBus>();
            var viewModel = new ShellViewModel();
            viewModel.InputText = "abc";
            viewModel.OnInputEnterCommand.Execute(null);
            Assert.That(viewModel.ShowText, Is.EqualTo("abc"));
        }

        [Test]
        public void ShouldShowSecondMessageInNewLine()
        {
            var viewModel = new ShellViewModel();
            viewModel.InputText = "abc";
            viewModel.OnInputEnterCommand.Execute(null);
            viewModel.InputText = "pawel";
            viewModel.OnInputEnterCommand.Execute(null);
            Assert.That(viewModel.ShowText, Is.EqualTo("abc\npawel"));

        }
    }
}
