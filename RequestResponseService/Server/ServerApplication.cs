﻿using Messaging;
using MongoDB.Driver;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Framing.v0_9_1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gadulec
{
    public sealed class Application : IDisposable
    {
        private DataSerializer serializer = new DataSerializer();
        private readonly IConnection connection;
        private readonly IModel channel;

        public Application()
        {
            var connectionFactory = new ConnectionFactory() { HostName = "localhost" };
            connection = connectionFactory.CreateConnection();
            channel = connection.CreateModel();

            channel.ExchangeDeclare("ggadulec", "direct", false, true, null);

            var incomingMessages = channel.QueueDeclare();
            channel.QueueBind(incomingMessages.QueueName, "ggadulec", AppConstants.ServerRoutingKey);

            var consumer = new DelegateConsumer<Message>(OnMessage, serializer);
            channel.BasicConsume(incomingMessages.QueueName, true, consumer);

        }

        public void Run()
        {
            Console.WriteLine("Press ENTER to save logs and exit");
            Console.ReadLine();
        }

        private void OnMessage(Message message, MessageContext context)
        {
            if (message.Text == "abc")
            {
                var msg = new Message { Text = "cba", Nick = "serwer", Date = DateTime.Now };
                channel.BasicPublish("ggadulec", context.ReplyTopic, new BasicProperties { CorrelationId = context.CorrelationId }, serializer.Serialize(msg));
            }

            //AddToDataBase(message);
            Console.WriteLine("{2} [{0}] - {1}", message.Nick, message.Text, message.Date.ToString("yyyy-MM-dd HH:mm:ss"));
        }

        public void AddToDataBase(Message message)
        {
            var connectionString = "mongodb://chatAdmin:poker333@ds053380.mongolab.com:53380/chatdata";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var database = server.GetDatabase("chatdata");

            var collection = database.GetCollection<Message>("logs");
            var entity = new Message {Date = message.Date, Nick = message.Nick, Text = message.Text };
            collection.Insert(entity);
        }


        public void Dispose()
        {
            channel.Dispose();
            connection.Dispose();
        }
    }
}
