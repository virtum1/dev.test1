﻿using Chat;
using Gadulec;
using Messaging;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace ChatClientTests
{
    [TestFixture]
    public class ShellViewModelTests
    {
        [Test]
        public void ShouldClearInputAfterEnter()
        {
            var sc = Substitute.For<ISynchronizationContext>();
            sc.When(o => o.Post(Arg.Any<Action>()))
                .Do(o => (o[0] as Action)());

            var bus = new LocalBus();
            var viewModel = new ShellViewModel(bus, sc);
            viewModel.InputText = "abc";
            viewModel.OnInputEnterCommand.Execute(null);
            Assert.That(viewModel.InputText, Is.Null);
        }

        [Test]
        public void ShouldShowMessageAfterEnter()
        {
            var bus = new LocalBus();
            var viewModel = new ShellViewModel(bus, null);
            viewModel.InputText = "abc";
            viewModel.OnInputEnterCommand.Execute(null);
            Assert.That(viewModel.MessagesHistory.First(), Is.EqualTo("abc"));
        }

        [Test]
        public void ShouldShowNewMessageInNewLine()
        {
            var bus = new LocalBus();
            var viewModel = new ShellViewModel(bus, null);
            viewModel.InputText = "abc";
            viewModel.OnInputEnterCommand.Execute(null);
            viewModel.InputText = "pawel";
            viewModel.OnInputEnterCommand.Execute(null);
            Assert.That(viewModel.MessagesHistory.First(), Is.EqualTo("abc\npawel"));
        }

        class LocalBus : IBus
        {
            private readonly Subject<NotifyMessage> channel = new Subject<NotifyMessage>();
            public Task<TResponse> Request<TRequest, TResponse>(TRequest request)
            {
                throw new NotImplementedException();
            }

            public IObservable<TNotification> On<TNotification>()
            {
               return channel as IObservable<TNotification>;
            }

            public void Publish<TNotification>(TNotification message)
            {
                channel.OnNext(message as NotifyMessage);
            }
        }
    }
}
