﻿using Gadulec;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RequestResposne
{
    [TestFixture]
    public class ChatTest
    {
        [Test]
        public void ShouldSerializeMessage()
        {
            var clientMessage = new Message { Text = "test text", Nick = "test nick", ApplicationToken = Guid.NewGuid() };
            var serializer = new DataSerializer();
            var serverMessage = (Message)serializer.Deserialize(serializer.Serialize(clientMessage), typeof(Message));

            Assert.That(clientMessage.Text, Is.EqualTo(serverMessage.Text));
            Assert.That(clientMessage.Nick, Is.EqualTo(serverMessage.Nick));
            Assert.That(clientMessage.ApplicationToken, Is.EqualTo(serverMessage.ApplicationToken));
        }
    }
}
