﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeBudgetMVVMProject
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Linq;
    using System.Linq.Expressions;

    public abstract class BaseModel : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        private PropertyChangedEventHandler propertyChangedHandler;
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add { propertyChangedHandler += value; }
            remove { propertyChangedHandler -= value; }
        }

        private EventHandler<DataErrorsChangedEventArgs> errorsChangedHandler;
        event EventHandler<DataErrorsChangedEventArgs> INotifyDataErrorInfo.ErrorsChanged
        {
            add { errorsChangedHandler += value; ; }
            remove { errorsChangedHandler -= value; }
        }

        protected void NotifyOfPropertyChange<TProperty>(Expression<Func<TProperty>> expression)
        {
            var memberExpression = (MemberExpression)expression.Body;
            var propertyName = memberExpression.Member.Name;

            if (propertyChangedHandler != null)
            {
                propertyChangedHandler(this, new PropertyChangedEventArgs(propertyName));
            }
            if (errorsChangedHandler != null)
            {
                errorsChangedHandler(this, new DataErrorsChangedEventArgs(propertyName));
            }
        }



        public IEnumerable GetErrors(string propertyName)
        {
            throw new NotImplementedException();
        }

        public bool HasErrors
        {
            get { throw new NotImplementedException(); }
        }
    }
}
