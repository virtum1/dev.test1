﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace HomeBudget
{
    public sealed class StringToIntValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            double result;
            if (double.TryParse(value as string, out result))
            {
                if (result > 0.00)
                    return ValidationResult.ValidResult;
            }
            return new ValidationResult(false, "Wpisywana wartość musi być liczbą dodatnią");

        }
    }
}
