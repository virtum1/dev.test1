﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace HomeBudgetMVVMProject
{
    public class ScrollableDataGrid : DataGrid
    {
        public IScrollProvider GetScrollProvider()
        {
            return base.OnCreateAutomationPeer() as IScrollProvider;
        }
    }
}
