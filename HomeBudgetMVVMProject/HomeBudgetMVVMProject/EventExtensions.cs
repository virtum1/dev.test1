﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeBudget
{
    public static class EventExtensions
    {
        public static void Raise(this PropertyChangedEventHandler handler, object sender, string propertyName)
        {
            if (handler == null) return;
            handler(sender, new PropertyChangedEventArgs(propertyName));
        }

        public static void Raise(this EventHandler handler, object sender)
        {
            if (handler == null) return;
            handler(sender, EventArgs.Empty);
        }

    }
}
