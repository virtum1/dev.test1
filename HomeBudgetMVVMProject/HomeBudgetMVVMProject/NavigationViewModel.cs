﻿using Castle.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;


namespace HomeBudget
{
    class NavigationViewModel : INotifyPropertyChanged, IInitializable
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private Balance balance;
        private ICommand changePageCommand;
        private IPageViewModel currentPageViewModel;
        private List<IPageViewModel> pageViewModels;

        public void Initialize()
        {

        }

        public NavigationViewModel(Balance balance, IEnumerable<IPageViewModel> pages)
        {
            this.balance = balance;
            using (var dbCtx = new DatabaseContext())
            {
               balance.Total = dbCtx.OperationsData.Sum(o => o.Kwota);
            }

            pages = pages
                .OrderBy(o => o.GetType() != typeof(HomeViewModel))
                .ThenByDescending(o => o.GetType() == typeof(PaymentViewModel))
                .ThenByDescending(o => o.GetType() == typeof(PaycheckViewModel))
                .ThenByDescending(o => o.GetType() == typeof(FilterViewModel)); ;
            PageViewModels.AddRange(pages);

            // Set starting page
            CurrentPageViewModel = pages.First();


            balance.Changed += (sender, e) =>
                {
                    ShowBalance = "Twoje saldo:\n" + e.Total + " zł";
                    PropertyChanged.Raise(this, "ShowBalance");
                };
        }

        public string ShowBalance
        {
            get;
            set;
        }

        public ICommand ChangePageCommand
        {
            get
            {
                if (changePageCommand == null)
                {
                    changePageCommand = new DelegateCommand<object>(
                        p => ChangeViewModel((IPageViewModel)p),
                        p => p is IPageViewModel);
                }

                return changePageCommand;
            }
        }

        public List<IPageViewModel> PageViewModels
        {
            get
            {
                if (pageViewModels == null)
                    pageViewModels = new List<IPageViewModel>();

                return pageViewModels;
            }
        }

        public IPageViewModel CurrentPageViewModel
        {
            get
            {
                return currentPageViewModel;
            }
            private set
            {
                currentPageViewModel = value;
                PropertyChanged.Raise(this, "CurrentPageViewModel");
            }
        }

        private void ChangeViewModel(IPageViewModel viewModel)
        {
            if (!PageViewModels.Contains(viewModel))
                PageViewModels.Add(viewModel);

            CurrentPageViewModel = PageViewModels
                .FirstOrDefault(vm => vm == viewModel);
        }
    }
}
