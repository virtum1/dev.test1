﻿namespace HomeBudgetMVVMProject
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics;
    using System.Linq;
    using FrameworkValidator = System.ComponentModel.DataAnnotations.Validator;

    /// <summary>
    /// Simplifies validation on an object.
    /// </summary>
    public static class Validator
    {
        /// <summary>
        /// Validates a property.
        /// </summary>
        /// <param name="item">The object to validate.</param>
        /// <returns>List of all validation errors or empty list, if <paramref name="item"/> is valid.</returns>
        public static IEnumerable<String> ValidationErrorsForProperty(object item, string memberName)
        {
            var validationContext = new ValidationContext(item, null, null);
            validationContext.MemberName = memberName;
            var validationResults = new Collection<ValidationResult>();
            var propertyInfo = item.GetType().GetProperty(memberName);

            var result = new List<String>();
            if (propertyInfo == null)
            {
                return result;
            }
            var propertyValue = propertyInfo.GetValue(item, new object[0]);
            FrameworkValidator.TryValidateProperty(propertyValue, validationContext, validationResults);

            foreach (var validationResult in validationResults)
            {
                result.Add(validationResult.ErrorMessage);
            }

            return result;
        }
        /// <summary>
        /// Validates an object.
        /// </summary>
        /// <param name="item">The object to validate.</param>
        /// <returns>List of all validation errors or empty list, if <paramref name="item"/> is valid.</returns>
        public static IEnumerable<String> ValidationErrors(object item)
        {
            var validationContext = new ValidationContext(item, null, null);
            var validationResults = new Collection<ValidationResult>();
            FrameworkValidator.TryValidateObject(item, validationContext, validationResults, true);

            var result = new List<String>();
            foreach (var validationResult in validationResults)
            {
                result.Add(validationResult.ErrorMessage);
            }

            return result;
        }

        /// <summary>
        /// Validates an object
        /// </summary>
        /// <param name="obj">The object to validate</param>
        /// <exception cref="ArgumentException"><paramref name="obj"/> has validation errors.</exception>
        [DebuggerHidden]
        public static void ValidateArgument(object obj)
        {
            if (!ValidationErrors(obj).Any()) return;

            throw new ArgumentException("Argument is not valid");
        }
    }
}
