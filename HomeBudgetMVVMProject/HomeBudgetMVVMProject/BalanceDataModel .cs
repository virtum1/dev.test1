﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeBudget
{
    public sealed class BalanceDataModel
    {
        public int Id { get; set; }
        public double Kwota { get; set; }
        public string Opis { get; set; }
        public DateTime Data { get; set; }
    }
}
