﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HomeBudget
{
    public class PaycheckViewModel : IPageViewModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand OnClickButtonCommand { get; set; }
        private Balance balance;
        private readonly DateTime date = DateTime.Now;

        public string Name
        {
            get
            {
                return "Wypłata";
            }
        }

        private double? paycheckValue;
        public double? PaycheckValue
        {
            get
            {
                return paycheckValue;
            }
            set
            {
                paycheckValue = value;
                if (PropertyChanged == null) return;
                PropertyChanged(this, new PropertyChangedEventArgs("PaycheckValue"));
            }
        }

        private string paycheckDescription;
        public string PaycheckDescription
        {
            get
            {
                return paycheckDescription;
            }
            set
            {
                paycheckDescription = value;
                if (PropertyChanged == null) return;
                PropertyChanged(this, new PropertyChangedEventArgs("PaycheckDescription"));
            }
        }

        private string paycheckDone;
        public string PaycheckDone
        {
            get
            {
                return paycheckDone;
            }
            set
            {
                paycheckDone = value;
                if (PropertyChanged == null) return;
                PropertyChanged(this, new PropertyChangedEventArgs("PaycheckDone"));
            }
        }

        public PaycheckViewModel(Balance balance)
        {
            this.balance = balance;
            OnClickButtonCommand = new AsyncCommand(OnPressButton);
        }

        public async Task OnPressButton()
        {
            await balance.AddToDatabaseAsync(-paycheckValue.Value, paycheckDescription, date);
            
            PaycheckValue = null;
            PaycheckDescription = string.Empty;
            PaycheckDone = "Wypłata została wykonana!";
        }
    }
}
