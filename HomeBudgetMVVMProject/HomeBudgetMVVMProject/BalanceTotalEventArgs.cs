﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeBudget
{
   public sealed class BalanceTotalEventArgs : EventArgs
    {
       public double Total { get; private set; }

       public BalanceTotalEventArgs(double total)
       {
           Total = total;
       }
    }
}
