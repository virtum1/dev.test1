﻿using AutoMapper;
using Caliburn.Micro;
using Castle.Core;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace HomeBudget
{
    public sealed class FilterViewModel : PropertyChangedBase, IPageViewModel
    {
        public ObservableCollection<BalanceModel> Tasks { get; set; }
        public ICommand OnActivatedCommand { get; private set; }
        public CollectionViewSource CVS { get; set; }
        public ICommand RefreshDataCommand { get; set; }
        private DateTime yearMonth;
        public string YearMonth { get { return yearMonth.ToString("yyyy MMMM"); } }
        public DelegateCommand<object> PrevYearMonthCommand { get; set; }
        public DelegateCommand<object> NextYearMonthCommand { get; set; }

        public FilterViewModel()
        {
            Initialize();
        }

        public string Name
        {
            get { return "Archiwum"; }
        }

        private void Initialize()
        {
            OnActivatedCommand = new DelegateCommand<object>(OnActivated);

            Tasks = new ObservableCollection<BalanceModel>();

            this.CVS = new CollectionViewSource();
            this.CVS.Source = this.Tasks;
            CVS.Filter += CVS_Filter;

            RefreshDataCommand = new DelegateCommand<object>(o => RefreshData());
            PrevYearMonthCommand = new DelegateCommand<object>(OnPrevYearMonth);
            NextYearMonthCommand = new DelegateCommand<object>(OnNextYearMonth);
            var now = DateTime.Now;
            yearMonth = new DateTime(now.Year, now.Month, 1);
        }

        void CVS_Filter(object sender, FilterEventArgs e)
        {
            var typedItem = e.Item as BalanceModel;
            if (typedItem.Data.Year != yearMonth.Year)
            {
                e.Accepted = false;
                return;
            }

            if (typedItem.Data.Month != yearMonth.Month)
            {
                e.Accepted = false;
                return;
            }

            e.Accepted = true;
        }

        private void RefreshData()
        {
            CVS.View.Refresh();
        }


        //private static BalanceModel CopyData(BalanceDataModel balanceDataModel)
        //{
        //    var result = new BalanceModel();
        //    result.Kwota = balanceDataModel.Kwota;
        //    result.Opis = balanceDataModel.Opis;
        //    result.Data = balanceDataModel.Data;
        //    return result;
        //}

        //private void OnActivated(object obj)
        //{
        //    Tasks.Clear();

        //    using (var dbCtx = new DatabaseContext())
        //    {
        //        var items = dbCtx.OperationsData
        //            .Select(CopyData);

        //        foreach (var item in items)
        //        {
        //            Tasks.Add(item);
        //        }
        //    }
        //}

        private static BalanceModel CopyData(BalanceDataModel balanceDataModel)
        {
            Mapper.CreateMap<BalanceDataModel, BalanceModel>();
            var result = Mapper.Map<BalanceModel>(balanceDataModel);
            return result;
        }

        private void OnActivated(object obj)
        {
            Tasks.Clear();

            using (var dbCtx = new DatabaseContext())
            {
                var items = dbCtx.OperationsData
                    .Select(CopyData);

                foreach (var item in items)
                {
                    Tasks.Add(item);
                }
            }
        }

        private void OnPrevYearMonth(object parameter)
        {
            ChangeYearMonth(-1);

        }
        private void OnNextYearMonth(object parameter)
        {
            ChangeYearMonth(1);
        }

        private void ChangeYearMonth(int monthsDelta)
        {
            yearMonth = yearMonth.AddMonths(monthsDelta);
            NotifyOfPropertyChange(() => YearMonth);
            RefreshData();
        }
    }
}
