﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;

namespace HomeBudget
{
    public class HomeViewModel : PropertyChangedBase, IPageViewModel
    {
        public string Name
        {
            get
            {
                return "Strona główna";
            }
        }
    }
}
