﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeBudget
{
    public sealed class DatabaseContext : DbContext
    {
        public DbSet<BalanceDataModel> OperationsData { get; set; }

        public DatabaseContext()
        {
            
        }
    }
}
