﻿namespace HomeBudgetMVVMProject
{
    using Caliburn.Micro;
    using System.ComponentModel.Composition;
    using System.Windows.Forms;

    public abstract class BaseScreen : Caliburn.Micro.Screen, IPartImportsSatisfiedNotification
    {
        protected override void OnActivate()
        {
            base.OnActivate();
        }

        void IPartImportsSatisfiedNotification.OnImportsSatisfied()
        {
            Initialize();
        }

        protected abstract void Initialize();

    }
}
