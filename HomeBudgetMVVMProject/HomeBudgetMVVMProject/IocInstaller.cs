﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeBudget
{
    public sealed class IocInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<NavigationViewModel>(),
                //Component.For<Balance>(),
                Classes.FromThisAssembly().BasedOn<IPageViewModel>().WithService.Base());
        }
    }
}
