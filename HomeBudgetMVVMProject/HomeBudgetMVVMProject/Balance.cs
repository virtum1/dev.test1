﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HomeBudget
{
    public sealed class Balance
    {
        public static Balance Default { get; private set; }
        public BalanceDataModel totalValues;

        private EventHandler<BalanceTotalEventArgs> changed;
        public event EventHandler<BalanceTotalEventArgs> Changed
        {
            add
            {
                changed += value;
                value(this, new BalanceTotalEventArgs(Total));
            }

            remove { changed -= value; }
        }

        public double Total { get; set; }
        public readonly List<BalanceDataModel> payments = new List<BalanceDataModel>();

        public Task AddToDatabaseAsync(double value, string description, DateTime date)
        {
            var dbCtx = new DatabaseContext();
            var operation = new BalanceDataModel { Kwota = value, Opis = description, Data = date };
            dbCtx.OperationsData.Add(operation);
            

            return dbCtx.SaveChangesAsync()
                .ContinueWith(o =>
                {
                    Thread.Sleep(10000);
                    Total = dbCtx.OperationsData.Sum(_ => _.Kwota);
                    RiseChanged();
                    dbCtx.Dispose();
                });
        }

        private void RiseChanged()
        {
            if (changed == null) return;
            changed(this, new BalanceTotalEventArgs(Total));
        }
    }
}
