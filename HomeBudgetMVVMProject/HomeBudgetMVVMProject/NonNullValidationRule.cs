﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace HomeBudget
{
    public sealed class NonNullValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var aaa = value as string;
            if (!string.IsNullOrEmpty(aaa)) return ValidationResult.ValidResult;

            return new ValidationResult(false, "Pole nie może byc puste");
        }
    }
}
