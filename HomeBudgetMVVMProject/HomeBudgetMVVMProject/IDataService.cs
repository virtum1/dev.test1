﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeBudgetMVVMProject
{
    public interface IDataService
    {
        List<BalanceEntry> GetThings();
    }
}
