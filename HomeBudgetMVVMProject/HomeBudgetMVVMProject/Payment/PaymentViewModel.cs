﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HomeBudget
{
    public class PaymentViewModel : IPageViewModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand OnClickButtonCommand { get; set; }
        private readonly Balance balance;
        private readonly DateTime date = DateTime.Now;

        public string Name
        {
            get
            {
                return "Wpłata";
            }
        }

        public string Text { get; set; }

        private double? paymentValue;
        public double? PaymentValue
        {
            get
            {
                return paymentValue;
            }
            set
            {
                paymentValue = value;
                if (PropertyChanged == null) return;
                PropertyChanged(this, new PropertyChangedEventArgs("PaymentValue"));
            }
        }

        private string paymentDescription;
        public string PaymentDescription
        {
            get
            {
                return paymentDescription;
            }
            set
            {
                paymentDescription = value;
                if (PropertyChanged == null) return;
                PropertyChanged(this, new PropertyChangedEventArgs("PaymentDescription"));
            }
        }

        private string paymentDone;
        public string PaymentDone
        {
            get
            {
                return paymentDone;
            }
            set
            {
                paymentDone = value;
                if (PropertyChanged == null) return;
                PropertyChanged(this, new PropertyChangedEventArgs("PaymentDone"));
            }
        }

        public PaymentViewModel(Balance balance)
        {
            this.balance = balance;
            OnClickButtonCommand = new AsyncCommand(OnPressedButton);
        }

        public async Task OnPressedButton()
        {
            await balance.AddToDatabaseAsync(paymentValue.Value, paymentDescription, date);
            PaymentValue = null;
            PaymentDescription = string.Empty;
            PaymentDone = "Wpłata została wykonana!";
        }
    }
}
