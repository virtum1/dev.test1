﻿namespace HomeBudgetMVVMProject
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    public interface IReportingService
    {
        Task SendServices(IEnumerable<BalanceEntry> services);

        Task<BalanceEntry[]> GetServices(int year, int month, CancellationToken cancellationToken);

        Task RemoveService(int serviceId);
    }
}
