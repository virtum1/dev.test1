﻿using HomeBudget;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HomeBudget.Tests
{
    [TestFixture]
    public sealed class AsyncCommandTests
    {
        [Test]
        public void ShouldWaitWhenExecuteFinish()
        {
            var tcs = new TaskCompletionSource<object>();
            var execute = tcs.Task;
            Func<Task> onExecute = () => execute;

            var command = new AsyncCommand(onExecute);

            Assert.That(command.CanExecute(null), Is.True);

            command.Execute(null);
            Assert.That(command.CanExecute(null), Is.False);

            tcs.SetResult(null);
            Assert.That(command.CanExecute(null), Is.True);
        }
    }
}
