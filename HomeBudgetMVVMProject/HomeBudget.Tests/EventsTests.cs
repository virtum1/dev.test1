﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeBudget.Tests
{
    [TestFixture]
    public sealed class EventsTests
    {
        [Test]
        public void ShouldUnregisterEvent()
        {
            var count  = 0;
            EventHandler handler = (s, ea) => { count++; };
            var publisher = new Publisher();
            publisher.OnInvoke += handler;
            publisher.Invoke();
            publisher.OnInvoke -= handler;
            publisher.Invoke();

            Assert.That(count, Is.EqualTo(1));
        }

        [Test]
        public void ShouldNotUnregisterAnonymousEvent()
        {
            var count = 0;
            var publisher = new Publisher();
            publisher.OnInvoke += (s, ea) => { count++; };
            publisher.Invoke();
            publisher.OnInvoke -= (s, ea) => { count++; };
            publisher.Invoke();

            Assert.That(count, Is.EqualTo(1));
        }

        class Publisher
        {
            public event EventHandler OnInvoke;

            public void Invoke()
            {
                if (OnInvoke == null) return;
                OnInvoke(this, EventArgs.Empty);
            }
        }
    }
}
