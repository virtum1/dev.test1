﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebPagesClickBot
{
    public sealed class ApplicationStartup
    {
        /// <summary>
        /// Create a key in windows registry.
        /// </summary>
        public void AddApplicationToStartup()
        {
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true))
            {
                key.SetValue("WebPagesClickBot", "\"" + Application.ExecutablePath + "\"");
            }
        }
    }
}
