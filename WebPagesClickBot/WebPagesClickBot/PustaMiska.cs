﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatiN.Core;

namespace WebPagesClickBot
{
   public sealed class PustaMiska
    {
       Browser browser;
       public PustaMiska(Browser browser)
       {
           this.browser = browser;
       }
       public void PustaMiskaClick()
       {
           Console.WriteLine("Wchodze w pusta miske");
           browser.GoTo("http://www.pmiska.pl");
           try
           {
               var image = browser.Image(Find.ByClass("a"));
               image.WaitForComplete();
               image.ClickNoWait();
               Console.WriteLine("Kliknięcie wykonane!");
           }
           catch
           {
               Console.WriteLine("Kliknięcie zostało dziś już wykonane");
           }
       }
    }
}
