﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WatiN.Core;


namespace WebPagesClickBot
{
    class Program
    {
        [STAThread]     
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;

            var startup = new ApplicationStartup();
            startup.AddApplicationToStartup();

            Console.WriteLine("Przygotowanie programu zajmie 2 minuty");
            Thread.Sleep(60000);
            Console.WriteLine("Została minuta");
            Thread.Sleep(50000);
            Console.WriteLine("Zostało: ");
            for (int i = 10; i > 0; i--)
            {
                Console.WriteLine(i);
                Thread.Sleep(1000);
            }

            using (var ie = new IE())
            {
                var pajacyk = new Pajacyk(ie);
                pajacyk.PajacykClick();

                Console.WriteLine("Kolejna strona otworzy się za:");
                for (int i = 10; i > 0; i--)
                {
                    Console.WriteLine(i);
                    Thread.Sleep(1000);
                }

                var pmiska = new PustaMiska(ie);
                pmiska.PustaMiskaClick();

                Console.WriteLine("Zakmnięcie programu za:");
                for (int i = 5; i > 0; i--)
                {
                    Console.WriteLine(i);
                    Thread.Sleep(1000);
                }
                Console.WriteLine("BOOM");
                Thread.Sleep(1000);
            }
        }
    }
}
