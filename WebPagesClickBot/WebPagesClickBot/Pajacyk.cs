﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatiN.Core;

namespace WebPagesClickBot
{
    public sealed class Pajacyk
    {
        Browser browser;
        public Pajacyk(Browser browser)
        {
            this.browser = browser;
        }

        public void PajacykClick()
        {
            Console.WriteLine("Wchodze w pajacyka");
            browser.GoTo("http://www.pah.org.pl/nasze-dzialania/8/pajacyk");
            try
            {
                var image = browser.Image(Find.ByClass("pajacyk-on"));
                image.WaitForComplete();
                image.ClickNoWait();
                //albo click zwykle
                Console.WriteLine("Klikniecie wykonane!");
            }
            catch
            {
                Console.WriteLine("Kliknięcie zostało dziś już wykonane");
            }
        }
    }
}
