﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace JsonClient2
{
    /// <summary>
    /// Allows to execute async methods.
    /// </summary>
    public sealed class AsyncCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private volatile bool canExecute = true;
        private readonly Func<Task> onExecute;
        public AsyncCommand(Func<Task> onExecute)
        {
            this.onExecute = onExecute;
        }

        public bool CanExecute(object parameter)
        {
            return canExecute;
        }

        public void Execute(object parameter)
        {
            canExecute = false;
            CanExecuteChanged.Raise(this);

            var task = onExecute();
            task.ContinueWith(o =>
            {
                canExecute = true;
                CanExecuteChanged.Raise(this);
            }, TaskContinuationOptions.ExecuteSynchronously);
        }
    }
}
