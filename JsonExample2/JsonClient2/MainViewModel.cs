﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace JsonClient2
{
    public sealed class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand SendClientButton { get; set; }
        public ICommand GetDatatButton { get; set; }
        HttpClient client = new HttpClient { BaseAddress = new Uri("http://localhost:49683") };
        Uri uri = new Uri("http://localhost:49683/api/values");

        private string firstName;
        public string FirstName
        {
            get
            {
                return firstName;
            }

            set
            {
                firstName = value;
                if (PropertyChanged == null) return;
                PropertyChanged(this, new PropertyChangedEventArgs("FirstName"));
            }
        }

        private string lastName;
        public string LastName
        {
            get
            {
                return lastName;
            }

            set
            {
                lastName = value;
                if (PropertyChanged == null) return;
                PropertyChanged(this, new PropertyChangedEventArgs("LastName"));
            }
        }

        private string info;
        public string Info
        {
            get
            {
                return info;
            }

            set
            {
                info = value;
                if (PropertyChanged == null) return;
                PropertyChanged(this, new PropertyChangedEventArgs("Info"));
            }
        }

        private string customersData;
        public string CustomersData
        {
            get
            {
                return customersData;
            }
            set
            {
                customersData = value;
                if (PropertyChanged == null) return;
                PropertyChanged(this, new PropertyChangedEventArgs("CustomersData"));
            }
        }

        public MainViewModel()
        {
            SendClientButton = new AsyncCommand(OnClientButtonPressed);
            GetDatatButton = new AsyncCommand(OnDataButtonPressed);
        }

        private async Task OnDataButtonPressed()
        {
            CustomersData = null;
            var response = await client.GetAsync(uri);
            var data = await response.Content.ReadAsStringAsync();
            CustomersData = data;
        }

        public async Task OnClientButtonPressed()
        {
            var item = "{ FirstName: '" + FirstName + "', LastName: '" + LastName + "'}";
            FirstName = null;
            LastName = null;

            var result = await client.PostAsync(uri, new StringContent(item, Encoding.UTF8, "application/json"));
            var created = await client.GetStringAsync(result.Headers.Location);
            Info += created + "\n";
        }


    }
}
