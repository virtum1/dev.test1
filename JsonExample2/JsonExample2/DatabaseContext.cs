﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace JsonExample2
{
    public class DatabaseContext : DbContext
    {
        public DbSet<PersonModel> Persons { get; set; }
    }
}