﻿using AutoMapper;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace JsonExample2.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<PersonModel> Get()
        {
            using (var ctx = new DatabaseContext())
            {
                return ctx.Persons.ToArray();
            }
        }

        // GET api/values/5
        public PersonModel Get(int id)
        {
            using (var ctx = new DatabaseContext())
            {
                return ctx.Persons.Where(o => o.Id == id).FirstOrDefault();
            }
        }

        // POST api/values
        public HttpResponseMessage Post([FromBody]PersonModel value)
        {
            List<PersonModel> personData = new List<PersonModel>();
            var id = 0;
            personData.Add(value);
            using (var ctx = new DatabaseContext())
            {
                var name = value.FirstName;
                var lastName = value.LastName;
                var person = new PersonModel { FirstName = name, LastName = lastName };
                ctx.Persons.Add(person);
                ctx.SaveChanges();
                id = person.Id;
            }

            var response = Request.CreateResponse(HttpStatusCode.Created, value);

            var uri = Url.Link("DefaultApi", new { id = id });
            response.Headers.Location = new Uri(uri);

            return response;
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        [HttpGet]
        public string Echo(string echo)
        {
            return echo;
        }
    }
}